#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 15 16:28:38 2020

MAIN PROGRAM TO TRAIN AND VALIDATE ANN IN KERAS USING CROSS VALIDATION FRO TIME2EVENT ANALYSIS
"""
import time
import numpy as np
import os
import gc
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import backend as K
from sklearn.model_selection import StratifiedKFold
#from numpy.random import seed
#seed(1)
#from tensorflow import set_random_seed
#set_random_seed(2)

# import self written modules
import config
if config.phase != 'train':
    raise Exception ('A training script is being run although in config.py phase is not set to train !' )
if config.task != 'time2event':
    raise Exception ('A time-to-event script is being run although in config.py task is not set to time-to-event!' )
import architecture
import plot_functions 
import metrics
import nnet_survival

# import settings from config.py
from config import inputs
from config import task
from config import batch_size 
from config import learning_rate 
from config import weight_decay
from config import momentum 
from config import dropout_rate
from config import num_epochs 
from config import n_folds 
from config import shuffle_folds
from config import opt
from config import num_neurons_hl
from config import time_span
from config import fixed_interval_width

date_string = time.strftime("%Y-%m-%d-%H:%M")  # time at which run is started. Used to save results
#%%
# Define the time intervals for the discrete-time survival model
# Since there are fewer events per day at later follow-up times, increasing
# time interval width at later times may improve parameter estimation.

if fixed_interval_width:
    print('Fixed interval width with time span ' + str(time_span) + ' years used.')
    breaks=np.arange(0.,365.*time_span,365//2)
    n_intervals=len(breaks)-1
    timegap = breaks[1:] - breaks[:-1]
else:
	halflife=365.*2
	breaks=-np.log(1-np.arange(0.0,0.96,0.05))*halflife/np.log(2) 
	n_intervals=len(breaks)-1
	timegap = breaks[1:] - breaks[:-1]
    
if shuffle_folds:
    print('Random selection of cv-folds is performed.')
    
#%%
# LOAD DATA

# set the network's inputs (clinical variables)
train_val_clinical = config.mcgill_clinical
    
# set the labels
train_val_labels = config.mcgill_labels

# set times  
train_val_times = config.mcgill_times
    
# Convert each patient's output data from (time, censoring indicator) format to a vector that 
# for each time interval specifies whether the patient survived that time interval, 
# and whether the patient failed during that time interval
train_val_surv_array = nnet_survival.make_surv_array(train_val_times, train_val_labels, breaks)

print('The loaded data has the following shape: ' + str(train_val_clinical.shape))
#%%
# TRAIN MODELS

# train model with CV
print('\n')
print('Training  model with ' +str(n_folds) + '-fold CV...')

kfold = StratifiedKFold(n_splits=n_folds, shuffle=shuffle_folds)

fold_counter = 1
for train, val in kfold.split(train_val_clinical, train_val_labels):
    print("==============================================")
    print("====== K-Fold Validation step => %d/%d =======" % (fold_counter,n_folds))
    print("==============================================")
        
    model = architecture.ANN_clinical(len_clinical=train_val_clinical[train].shape[1], num_neurons_hl=num_neurons_hl, \
                                      dropout=dropout_rate, l2=weight_decay, task=task, n_intervals=n_intervals)
    
    if opt == 'SGD':
        optimizer = SGD(lr=learning_rate, momentum=momentum)
    if opt == 'Adam':
        optimizer= Adam(lr=learning_rate)
    
    model.compile(loss=nnet_survival.surv_likelihood(n_intervals), \
                  optimizer=optimizer)
    
    if fold_counter == 1:
        model.summary()
        
    # paths to save results
    results_path = config.results_path_time2event + '/ANN/runs_keras_' + \
                    str(config.endpoint) + '/' + date_string + '/fold_' + str(fold_counter)
    
    if opt == 'SGD':                 
        parameters_path = '/' + date_string + '_years' + str(time_span) +'lr' + str (learning_rate) + 'momentum' + str(momentum) + \
                            'wd' + str(weight_decay) + 'dropout' + str(int(dropout_rate*100)) + \
                            'batchsize' + str(batch_size) + 'neurons_hl' + str(num_neurons_hl) + \
                            'foldshuffle' + str(shuffle_folds) 
    if opt == 'Adam':                 
        parameters_path = '/' + date_string + '_years' + str(time_span) + 'lr' + str (learning_rate) + \
                            'wd' + str(weight_decay) + 'dropout' + str(int(dropout_rate*100)) + \
                            'batchsize' + str(batch_size) + 'neurons_hl' + str(num_neurons_hl) + \
                            'foldshuffle' + str(shuffle_folds) 
                            
    # create folder to save results
    os.makedirs(results_path, exist_ok=True)
    
    # save training and validation indices for the different folds
    np.savetxt(results_path + '/train_idx', train)
    np.savetxt(results_path + '/val_idx', val)


    # save current best model according to HCI_3yr
    checkpoint_best = metrics.CheckpointHCI(train_data=train_val_clinical[train], val_data=train_val_clinical[val], breaks=breaks, \
                                        train_times=train_val_times[train], train_events=train_val_labels[train], \
                                        val_times=train_val_times[val], val_events=train_val_labels[val], inputs=inputs, \
                                        filepath = results_path + \
                                        '/best_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_HCI_3yr_{val_hci_3yr:.3f}.hdf5', \
                                        monitor='val_hci_3yr', verbose=1, \
                                        save_best_only=True, save_weights_only=True, mode='max')
    
    checkpoint_periodic = metrics.CheckpointHCI(train_data=train_val_clinical[train], val_data=train_val_clinical[val], breaks=breaks, \
                                        train_times=train_val_times[train], train_events=train_val_labels[train], \
                                        val_times=train_val_times[val], val_events=train_val_labels[val], inputs=inputs, \
                                        filepath = results_path + \
                                        '/periodic_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_HCI_3yr_{val_hci_3yr:.3f}.hdf5', \
                                        monitor='val_hci_3yr', verbose=1, \
                                        save_best_only=False, save_weights_only=True, period=500)
 
    early_stopping = EarlyStopping(monitor='val_hci_3yr', patience=1000, mode='max')
    
    history=model.fit(train_val_clinical[train], train_val_surv_array[train], \
                      validation_data = (train_val_clinical[val], train_val_surv_array[val]), \
                      batch_size=batch_size, epochs=num_epochs, \
                      callbacks=[checkpoint_best, checkpoint_periodic, early_stopping], \
                      verbose=2)

    # select histories to plot & save
    plot_functions.plot_histories(history=history, results_path=results_path, parameters_path=parameters_path, \
                                  fold_counter=fold_counter, \
                                  grid=True, opt=opt, \
                                  loss=True, auc=False, \
                                  hci_1yr=False, hci_3yr=True)

    # clear some (GPU) memory and reset layer, metrics, etc
    K.clear_session()
    gc.collect()
    del checkpoint_best
    del checkpoint_periodic
    del early_stopping
    
    fold_counter += 1

    #print(train_labels_cv[0:5])
