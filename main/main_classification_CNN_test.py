#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 11:27:40 2020

@author: Elia.Lombardo

MAIN PROGRAM TO TEST MODELS AND GET CONFIDENCE INTERVALS WITH BOOTSTRAP RESAMPLING
"""

import numpy as np
import os
#import tensorflow.keras.backend as K

# import self written modules
import config
if config.phase != 'test':
    raise Exception ('A testing script is being run although in config.py phase is not set to test !' )
if config.task != 'classification':
    raise Exception ('A classification script is being run although in config.py task is not set to classification!' )
import architecture
from metrics import get_roc_metrics
import data_generators
import load_images
from plot_functions import compute_stats_and_plot_hist, compute_pvalue_and_plot_km
from utils import subdir_paths, get_validation_threshold

from config import task
from config import inputs
from config import clinical_var
from config import cohort
from config import trained_models
from config import bootstrap_resampling
from config import bootstrap
from config import dim
from config import channels
from config import crop_size
from config import binary_mask

if clinical_var == False:
    models_path = config.results_path_classification +  '/CNN/runs_keras_' + \
                    str(config.endpoint) + '_' + str(dim) + 'D/' + trained_models
    
    results_path = config.results_path_classification +  '/CNN/tests_keras_' + \
                str(config.endpoint) + '_' + str(dim) + 'D/' \
                + trained_models + '_bootstrap/'
else:
    models_path = config.results_path_classification +  '/CNNplusClinical/runs_keras_' + \
                    str(config.endpoint) + '_' + str(dim) + 'D/' + trained_models

    results_path = config.results_path_classification +  '/CNNplusClinical/tests_keras_' + \
                    str(config.endpoint) + '_' + str(dim) + 'D/' \
                    + trained_models + '_bootstrap/'  
                    
os.makedirs(results_path, exist_ok=True)
#%%
#LOAD DATA
print('\n')
print('Loading the data...')

if cohort == 'maastro':
    data, labels = load_images.load_data(config.maastro_paths_masked, config.maastro_labels, \
                                         binary=binary_mask, dim=dim)

        
else:
    raise Exception('Write here other testing cohorts of choice.')
          
if clinical_var:
    if cohort == 'maastro':
        clinical = config.maastro_clinical
            
print('The loaded data has the following shape: ' + str(data.shape) + str('\n')) 
# (full_b, c, depth, height, width)
#%%
# LOAD BEST MODELS FOR GIVEN CROSS VALIDATION        
 
best_models = []                   
val_indices = []
# loop over different folds
for fold in subdir_paths(models_path):
    aucs = []
    model_files = [] 
    #print(fold)

    # loop over all subfolders and files of one cross validation fold
    for dirName, subdirList, fileList in os.walk(fold):
        for file in fileList:
            if file[-5:] == '.hdf5':
                model_files.append(file)
                # append all the AUC values
                aucs.append(float(file[-10:-5]))
                
    aucs = np.array(aucs)
    model_files = np.array(model_files)
    
    # find best model by looking at the largest AUC for the given fold
    best_model_of_fold = model_files[np.argmax(aucs)]
    print(fold + '/' + best_model_of_fold)
    path_to_best_model = fold + '/' + best_model_of_fold 
    best_models.append(path_to_best_model)       
    val_indices.append(np.loadtxt(fold + '/val_idx'))   


# build and load best models form cross validation        
models = []
if dim == 2:
    for model_path in best_models:   
        if clinical_var == False:
            model = architecture.CNN_2D(channels=channels, crop_size=crop_size, \
                                dropout_rate=0, task=task)  
        else:
            model = architecture.CNN_plus_clinical_2D(channels=channels, crop_size=crop_size, \
                                              len_clinical=clinical.shape[1], \
                                              dropout_rate=0, task=task) 
        model.load_weights(model_path)   
        models.append(model)
    
if dim == 3:
    for model_path in best_models:   
        if clinical_var == False:
            model = architecture.CNN_3D(channels=channels, crop_size=crop_size, \
                                dropout_rate=0, task=task)  
        else:
            model = architecture.CNN_plus_clinical_3D(channels=channels, crop_size=crop_size, \
                                              len_clinical=clinical.shape[1], \
                                              dropout_rate=0, task=task) 
        model.load_weights(model_path)   
        models.append(model)

#%%
# TEST MODELS AND USE BOOTSTRAP RESAMPLING TO GET CONFIDENCE INTERVALS

if bootstrap_resampling:
    # bootstrap resampling
    auc_scores = list()
    for sample_counter in range(bootstrap):
        print("==============================================")
        print("====== Current bootstrap sample => %d/%d =======" % (sample_counter+1,bootstrap))
        print("==============================================")
        
        # bootstrap sample indices
        indices = np.random.randint(0, len(data), len(data))
        #print('Indices of current bootstrap sample: ' + str(indices))
        
        
        if clinical_var == False:
            test_gen_sample = data_generators.ValDataGenerator((data[indices], labels[indices]), \
                                                     batch_size=1, crop_size=crop_size, \
                                                     shuffle=False, prediction=True, clinical=clinical_var)    
        else:
            test_gen_sample = data_generators.ValDataGenerator((data[indices], labels[indices], clinical[indices]), \
                                                     batch_size=1, crop_size=crop_size, \
                                                     shuffle=False, prediction=True, clinical=clinical_var) 
        
        # loop over best validation models
        preds = []    
        for fold in range(len(best_models)):
    
            pred = models[fold].predict(test_gen_sample, \
                                         workers=12, use_multiprocessing=False, verbose=2)
            
            #print('Predictions: ' + str(pred))
            #print(len(pred))
            #print(len(labels))
            
            preds.append(pred)
            
        # model averaging i.e. averaging the predictions
        averaged_pred = np.mean(preds, axis=0) 
        # get averaged roc metrics
        averaged_fpr, averaged_tpr, averaged_thresholds, \
        averaged_roc_auc = get_roc_metrics(labels[indices], averaged_pred)   
        print('ROC AUC: ' + str(averaged_roc_auc))

        # store statistic
        auc_scores.append(averaged_roc_auc) 
    
    compute_stats_and_plot_hist(scores=auc_scores, path=results_path, \
                                cohort=cohort, task=task, bootstrap=bootstrap)
    
    # save scores
    np.savetxt(results_path + '/bootstrap_model_averaged_auc_scores_' + cohort, auc_scores)

#%% 
#KAPLAN-MEIER ANALYSIS ON TRUE TEST SET

# times are needed for KM analysis
if cohort == 'maastro':
    times = config.maastro_times

# compute model averaged auc for 'real' (not bootstrapped) testing set
if clinical_var == False:
    test_gen_sample = data_generators.ValDataGenerator((data, labels), batch_size=1, \
                                            crop_size=crop_size, \
                                            shuffle=False, prediction=True, clinical=clinical_var)    
else:
    test_gen_sample = data_generators.ValDataGenerator((data, labels, clinical), batch_size=1, \
                                            crop_size=crop_size, \
                                            shuffle=False, prediction=True, clinical=clinical_var) 


preds = []        
# loop over different best models
for fold in range(len(best_models)):
    
    models[fold].summary()
    #print('Metrics in model: ' + str(models[fold].metrics_names))

    pred = models[fold].predict(test_gen_sample, workers=12, use_multiprocessing=False, verbose=1)
    
    #print('Predictions: ' + str(pred))
    #print(len(pred))
    #print(len(labels))
    
    preds.append(pred)

# model averaging i.e. averaging the predictions
averaged_pred = np.mean(preds, axis=0) 

# get roc metrics
averaged_fpr, averaged_tpr, averaged_thresholds, \
averaged_auc = get_roc_metrics(labels, averaged_pred) 
print('AUC for real test set: ' + str(averaged_auc))
with open(results_path + '/real_model_averaged_auc_' + cohort, 'w') as f:
    f.write('Model averaged AUC for real test set: \n' + str(averaged_auc) + '\n')
# delete test data to free up some space on CPU
del data

print('\nLoading validation data...')
# load data from validation cv splitting to find threshold by averaging over the folds
train_val_data, train_val_labels = load_images.load_data(config.mcgill_paths_masked, config.mcgill_labels, \
                                                         binary=binary_mask, dim=dim)

# get threshold from validation models and data
if clinical_var == False:

    threshold = get_validation_threshold(data=train_val_data, labels=train_val_labels, clinical=[], \
                                         val_indices=val_indices, \
                                         models=models, n_folds=len(best_models), inputs=inputs, \
                                         clinical_var=clinical_var)
else:
    train_val_clinical = config.mcgill_clinical

    threshold = get_validation_threshold(data=train_val_data, labels=train_val_labels, clinical=train_val_clinical, \
                                         val_indices=val_indices, \
                                         models=models, n_folds=len(best_models), inputs=inputs, \
                                         clinical_var=clinical_var)

print('(Validation) Threshold used for stratification: ' + str(threshold))
with open(results_path + '/validation_threshold', 'w') as f:
    f.write('Threshold from validation: \n' + str(threshold) + '\n')


########################
# TO BE DELETED: setting temp threshold that works with such a small number of testing patients
print(averaged_pred)
threshold = 0.4323
########################

# find high and low risk groups using the threshold
#print(np.shape(averaged_pred)) # (136,1) for e.g. maastro
high_risk_patient_indices = np.where(averaged_pred > threshold)
low_risk_patient_indices = np.where(averaged_pred < threshold)
print('Number of high risk patients: ' + str(len(high_risk_patient_indices[0])))

# for each of the two groups, plot true survival distribution (Kaplan-Meier estimator)
compute_pvalue_and_plot_km(times, labels, high_risk_patient_indices[0], low_risk_patient_indices[0], \
                           endpoint=config.endpoint, path=results_path, cohort=cohort)  
