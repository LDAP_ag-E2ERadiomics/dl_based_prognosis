#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 15 16:28:38 2020

MAIN PROGRAM TO PERFORM GRID SEARCH FOR ANN HYPERPARAMETERS IN TIME2EVENT ANALYSIS
"""
import time
import numpy as np
import os
import gc
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import backend as K
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import ParameterGrid

#from numpy.random import seed
#seed(1)
#from tensorflow import set_random_seed
#set_random_seed(2)

# import self written modules
import config
if config.phase != 'train':
    raise Exception ('A training script is being run although in config.py phase is not set to train !' )
if config.task != 'time2event':
    raise Exception ('A time-to-event script is being run although in config.py task is not set to time-to-event!' )
import architecture
import metrics
import nnet_survival

# import settings from config.py
from config import inputs
from config import task
from config import batch_size 
from config import momentum 
from config import num_epochs 
from config import opt
from config import n_folds 
from config import shuffle_folds
from config import time_span
from config import fixed_interval_width

parameters = {'dropout': [0, 0.25], \
              'l2':[1e-4, 1e-3], \
              'lr': [1e-4, 1e-3], \
              'neurons': [12, 14], \
              'val_ci_3yr': [0],}

if shuffle_folds:
    print('Random selection of cv-folds is performed.')
    
date_string = time.strftime("%Y-%m-%d-%H:%M")  # time at which run is started. Used to save results
#%%
# Define the time intervals for the discrete-time survival model
# Since there are fewer events per day at later follow-up times, increasing
# time interval width at later times may improve parameter estimation.

if fixed_interval_width:
    print('Fixed interval width with time span ' + str(time_span) + ' years used.')
    breaks=np.arange(0.,365.*time_span,365//2)
    n_intervals=len(breaks)-1
    timegap = breaks[1:] - breaks[:-1]
else:
	halflife=365.*2
	breaks=-np.log(1-np.arange(0.0,0.96,0.05))*halflife/np.log(2) 
	n_intervals=len(breaks)-1
	timegap = breaks[1:] - breaks[:-1]
    
if shuffle_folds:
    print('Random selection of cv-folds is performed.')
    
#%%
# LOAD DATA

# set the network's inputs (clinical variables)
train_val_clinical = config.mcgill_clinical
    
# set the labels
train_val_labels = config.mcgill_labels

# set times  
train_val_times = config.mcgill_times
    
# Convert each patient's output data from (time, censoring indicator) format to a vector that 
# for each time interval specifies whether the patient survived that time interval, 
# and whether the patient failed during that time interval
train_val_surv_array = nnet_survival.make_surv_array(train_val_times, train_val_labels, breaks)

print('The loaded data has the following shape: ' + str(train_val_clinical.shape))
#%%
# cross-validation to find best hyper parameters
print('\n')
print('Performing grid search with cross validation...')

# create list with combination of hyperparameters and correpsonding entry for metric set to zero
list_params = list(ParameterGrid(parameters))
print(list_params)
print('\n')

kfold = StratifiedKFold(n_splits=n_folds, shuffle=shuffle_folds)

# initialize array where as many metric values as n_folds for one combination will be written
grid_search_val = np.zeros((1,n_folds))

for combination in range(len(list_params)):
    print("==============================================")
    print("====== Current Combination => %d/%d =============" % (combination+1,len(list_params)))
    print("==============================================")
    
    fold_counter = 1
    for train, val in kfold.split(train_val_clinical, train_val_labels):
        print("==============================================")
        print("====== K-Fold Validation step => %d/%d =======" % (fold_counter,n_folds))
        print("==============================================")
        
        model = architecture.ANN_clinical(len_clinical=train_val_clinical[train].shape[1], \
                                          num_neurons_hl=list_params[combination]['neurons'],\
                                          dropout=list_params[combination]['dropout'], \
                                          l2=list_params[combination]['l2'], task=task, n_intervals=n_intervals)

        if opt == 'SGD':
            optimizer = SGD(lr=list_params[combination]['lr'], momentum=momentum)
        if opt == 'Adam':
            optimizer= Adam(lr=list_params[combination]['lr'])
            
        model.compile(loss=nnet_survival.surv_likelihood(n_intervals), \
                      optimizer=optimizer)
        
        # paths to save results
        results_path = config.results_path_time2event + '/ANN/runs_keras_' + \
                        str(config.endpoint) + '/' + date_string + '_grid_search/combination_' + str(combination) \
                        + '/fold_' + str(fold_counter)
        
        if opt == 'SGD':                 
            parameters_path = '/' + date_string + 'years' + str(time_span) +'_lr' + str (list_params[combination]['lr']) + \
                                'momentum' + str(momentum) + \
                                'wd' + str(list_params[combination]['l2']) + 'dropout' + str(int(list_params[combination]['dropout']*100)) + \
                                'batchsize' + str(batch_size) + 'neurons_hl' + str(list_params[combination]['neurons']) + \
                                'foldshuffle' + str(shuffle_folds) 
        if opt == 'Adam':                 
            parameters_path = '/' + date_string + 'years' + str(time_span) + '_lr' + str (list_params[combination]['lr']) + \
                                'wd' + str(list_params[combination]['l2']) + 'dropout' + str(int(list_params[combination]['dropout']*100)) + \
                                'batchsize' + str(batch_size) + 'neurons_hl' + str(list_params[combination]['neurons']) + \
                                'foldshuffle' + str(shuffle_folds) 
                                
        # create folder to save results
        os.makedirs(results_path, exist_ok=True)
        
        # save training and validation indices for the different folds
        np.savetxt(results_path + '/train_idx', train)
        np.savetxt(results_path + '/val_idx', val)        
        
        # save current best model according to CI_3yr
        checkpoint_best = metrics.CheckpointHCI(train_data=train_val_clinical[train], val_data=train_val_clinical[val], breaks=breaks, \
                                            train_times=train_val_times[train], train_events=train_val_labels[train], \
                                            val_times=train_val_times[val], val_events=train_val_labels[val], inputs=inputs, \
                                            filepath = results_path + \
                                            '/best_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_HCI_3yr_{val_hci_3yr:.3f}.hdf5', \
                                            monitor='val_hci_3yr', verbose=2, \
                                            save_best_only=True, save_weights_only=True, mode='max')

        checkpoint_periodic = metrics.CheckpointHCI(train_data=train_val_clinical[train], val_data=train_val_clinical[val], breaks=breaks, \
                                            train_times=train_val_times[train], train_events=train_val_labels[train], \
                                            val_times=train_val_times[val], val_events=train_val_labels[val], inputs=inputs, \
                                            filepath = results_path + \
                                            '/periodic_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_HCI_3yr_{val_hci_3yr:.3f}.hdf5', \
                                            monitor='val_hci_3yr', verbose=2, \
                                            save_best_only=False, save_weights_only=True, period=500)
        
        early_stopping = EarlyStopping(monitor='val_hci_3yr', patience=1000, mode='max')

        history=model.fit(train_val_clinical[train], train_val_surv_array[train], \
                          validation_data = (train_val_clinical[val], train_val_surv_array[val]), \
                          batch_size=batch_size, epochs=num_epochs, \
                          callbacks=[checkpoint_best, checkpoint_periodic, early_stopping], \
                          verbose=2)

        hcis = []
        model_files = [] 
        # loop over all subfolders and files of one cross validation fold to load best model
        for dirName, subdirList, fileList in os.walk(results_path):
            for file in fileList:
                if file[-5:] == '.hdf5':
                    model_files.append(file)
                    # append all the AUC values
                    hcis.append(float(file[-10:-5]))
                    
        hcis = np.array(hcis)
        model_files = np.array(model_files)
        
        # find best model by looking at the largest AUC for the given fold
        best_model_of_fold = model_files[np.argmax(hcis)]
        path_best_model = os.path.join(results_path, best_model_of_fold)
        print('Path to best model of fold: ' + str(path_best_model))
        
        # load weights of best model
        model =  architecture.ANN_clinical(len_clinical=train_val_clinical[train].shape[1], \
                                          num_neurons_hl=list_params[combination]['neurons'],\
                                          dropout=list_params[combination]['dropout'], \
                                          l2=list_params[combination]['l2'], task=task, n_intervals=n_intervals)
        model.load_weights(path_best_model)   
    
        # perform prediction with best model and save HCI        
        y_val_predicted = np.asarray(model.predict(train_val_clinical[val], verbose=0))
        val_hci_3yr = metrics.hci(y_val_predicted, breaks, train_val_times[val], train_val_labels[val], 3)
        grid_search_val[0,fold_counter-1] = val_hci_3yr

        # clear some (CPU/GPU) memory and reset layer, metrics etc counters
        K.clear_session()
        gc.collect()
        del checkpoint_best
        del checkpoint_periodic
        del early_stopping
       
        fold_counter += 1
    
    print('Average of 3 folds for one combination: ' + str(np.average(grid_search_val,axis=1)[0]))    
    list_params[combination]['val_ci_3yr'] = np.average(grid_search_val,axis=1)[0] # [0] needed to selected value and not array with the value

# save resulting list with 3-fold averaged CIs for every combination of hyperparameters
pathSaving = config.results_path_time2event + '/ANN/runs_keras_' + \
                str(config.endpoint) + '/' + date_string + '_grid_search/'
with open(pathSaving + 'grid_search_output_list.txt', 'w') as f:
    for combination in range(len(list_params)):
        print(list_params[combination], file=f)
        