#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 5 09:03:10 2020

@author: Elia.Lombardo

Configuration file for main time2event and classification scripts
"""

# path to project folder (to be adapted to your project download location)
base_path = '/project/med5/End2EndRadiomics/Public_Gitlab_Repo/dl_based_prognosis'
# path to results folder
results_path_classification = base_path + '/results/classification'
results_path_time2event = base_path + '/results/time2event'


import numpy as np
import warnings
# ignoring elementwise comparison future warnings 
# (https://stackoverflow.com/questions/40659212/futurewarning-elementwise-comparison-failed-returning-scalar-but-in-the-futur) 
warnings.simplefilter(action='ignore', category=FutureWarning)

# add project auxiliary folder to your Python path to be able to import self written modules
import sys
sys.path.append(base_path + '/auxiliary')

# importing self written module from auxiliray folder
import load_clinical
#%%
# SET MODEL SETTINGS

# task, endpoint, etc settings
phase = 'test' # train, test
inputs = 'imaging' # imaging, clinical_variables
if inputs == 'imaging':
    dim = 2                  # 2D or 3D CNN
    channels = 1               # num of input channels of network, only =1 possible
    clinical_var = False   # whether to use CNN (False) or CNN+clinical (True)
    binary_mask = False      # whether to perform binary masking experiment (True) or not (False)  
else:
    clinical_var = True  # has to be True for clinical_varibales ANN
task = 'time2event' # classification , time2event
time_start = 'rt_end' # diagnosis, rt_end --> for time2event
endpoint = 'DM' 



if phase == 'train':
    if inputs == 'imaging':
        if task == 'classification':
            batch_size = int(2) # int(4) # int(32)
            crop_size = 128              # size to which image is cropped  (i.e. patch_size)
            shift_range = 0.2            # shift applied during random crop in percentage [0,0.5] of the original size (256)
            patch_center_dist_from_border = 256//(1/(0.5 - shift_range)) # e.g. 76 for shift_range = 0.2 (i.e. cube of 52x52x52 around center is the limit for the center of the 128 crop)
            rot_max = 60                 # in degree [0,90]
            deformation_max = 0.25       # maximal deformation [0,1] relative to the patch_size 
            prob_rot = 0.1
            prob_def = 0.1
            mirroring = True
            learning_rate = 2e-4
            momentum = 0.5
            weight_decay = 1e-4
            dropout_rate = 0.25
            num_epochs = 2 # 500
            n_folds = 3
            shuffle_folds = True
            opt = 'Adam'              # 'Adam' or 'SGD' available
            
        if task == 'time2event':
            batch_size = int(2) # int(4) 
            crop_size = 128            # size to which image is cropped  (i.e. patch_size)
            shift_range = 0.2          # shift applied during random crop in percentage [0,0.5] of the original size (256)
            patch_center_dist_from_border = 256//(1/(0.5 - shift_range)) # e.g. 76 for shift_range = 0.2 (i.e. cube of 52x52x52 around center is the limit for the center of the 128 crop)
            rot_max = 60               # in degree [0,90]
            deformation_max = 0.25     # maximal deformation [0,1] relative to the patch_size 
            prob_rot = 0.2
            prob_def = 0.2
            mirroring = True
            learning_rate = 3e-4
            momentum = 0.5
            weight_decay = 1e-4
            dropout_rate = 0.25
            num_epochs = 3 # 300
            n_folds = 3
            shuffle_folds = True
            opt = 'Adam'              # 'Adam' or 'SGD' available
            time_span = 5              # in years
            fixed_interval_width = True
        
    if inputs == 'clinical_variables':
        if task == 'classification':
            batch_size = int(2) # int(64) 
            learning_rate = 1e-4
            momentum = 0.5
            weight_decay = 1e-3
            dropout_rate = 0.25
            num_neurons_hl = 14
            num_epochs = 5 # 5000
            n_folds = 3
            shuffle_folds = True
            opt = 'Adam' 
        if task == 'time2event':
            batch_size = int(2) # int(64) 
            learning_rate = 1e-3
            momentum = 0.5
            weight_decay = 1e-3
            dropout_rate = 0
            num_neurons_hl = 14
            num_epochs = 3 # 3000
            n_folds = 3
            shuffle_folds = True
            opt = 'Adam' 
            time_span = 5              # in years
            fixed_interval_width = True
            
if phase == 'test':
    if inputs == 'imaging':
        if task == 'classification':
            cohort = 'maastro' # maastro, cro, pmcc
            trained_models = '2020-11-11-16:29'  # folder to CV models to be loaded for testing
            bootstrap_resampling = True
            bootstrap =  10 # 1000
            channels = 1
            crop_size = 128
        if task == 'time2event':
            cohort = 'maastro'
            trained_models = '2021-01-19-09:03' # folder to CV models to be loaded for testing
            bootstrap_resampling = True
            bootstrap = 10
            channels = 1
            crop_size = 128
            time_span = 5              # in years
            fixed_interval_width = True 
            
    if inputs == 'clinical_variables':
        if task == 'classification':
            cohort = 'maastro'
            trained_models = '2020-11-11-14:48' # folder to CV models to be loaded for testing
            num_neurons_hl = 14
            bootstrap_resampling = True
            bootstrap = 10 # 1000  
        if task == 'time2event':
            cohort = 'maastro'
            trained_models = '2020-11-16-08:50'  # folder to CV models to be loaded for testing
            num_neurons_hl = 14
            bootstrap_resampling = True
            bootstrap = 10 # 1000
            time_span = 5              # in years
            fixed_interval_width = True 
      
            
print('========= Settings Summary =========')
print('Phase: ' + str(phase))
print('Using ' + str(endpoint) + ' as endpoint.')
print('Performing a ' + task + ' task.')
print('Using ' + str(inputs) + ' inputs.')
if inputs == 'imaging':
    print('Using a ' + str(dim) + '-D CNN.')
if (clinical_var == True) and (inputs != 'clinical_variables'):
    print('Using CNN+Clinical.')
if (inputs == 'imaging') and (binary_mask == True):
    print('Performing binary masking experiment on input CT.')     
if phase == 'test':
    print('Using ' + cohort + ' test cohort.')
print('\n')
#%%
# SET THE PATHS FOR THE LABELS AND LOAD THEM

# set cohort dictionaries
path_to_labels_mcgill = {'name': 'mcgill', \
                         'path' : base_path + '/data/preprocessed/McGill/clinical_data_McGill.xlsx', \
                         'patients_nr': [3, 2, 6, 2]} # actually [87, 101, 41, 65]
path_to_labels_maastro = {'name': 'maastro', \
                          'path' : base_path + '/data/preprocessed/MAASTRO/clinical_data_MAASTRO.xlsx', \
                          'patients_nr': 6} # actually 136

# group all label dictionaries in list
path_to_labels_all = [path_to_labels_mcgill, path_to_labels_maastro]

# set excel sheet column number for labels 
if endpoint == 'DM':
    # Patient got distant metastasis if dm=1 
    label_to_extract_mcgill = 19
    label_to_extract_maastro = 27
    
# load the selected labels    
labels_HGJ, labels_CHUS, labels_HMR, labels_CHUM, labels_HN1_MAASTRO  = \
            load_clinical.load_labels(path_to_labels_all,label_to_extract_mcgill,label_to_extract_maastro)

# group labels of subcohorts together and change label type
#train_labels = np.concatenate((labels_HGJ, labels_CHUS)).astype(np.float32) # splitting used in Diamant et al
#val_labels = np.concatenate((labels_HMR, labels_CHUM)).astype(np.float32)
mcgill_labels = np.concatenate((labels_HGJ, labels_CHUS, labels_HMR, labels_CHUM)).astype(np.float32)
maastro_labels = labels_HN1_MAASTRO.astype(np.float32)


# times are needed  when performing time2event analysis or to to Kaplan-Meier plots 
if endpoint == 'DM':
    # time of event
    if time_start == 'diagnosis':
        time_to_extract_mcgill = 22 
        time_to_extract_maastro = 28
    if time_start == 'rt_end':
        time_to_extract_mcgill = 25 
        time_to_extract_maastro = 29
 
# load selected times        
times_HGJ, times_CHUS, times_HMR, times_CHUM, times_HN1_MAASTRO = \
            load_clinical.load_times(path_to_labels_all,time_to_extract_mcgill, time_to_extract_maastro)

#train_times = np.concatenate((times_HGJ, times_CHUS)).astype(np.float32) 
#val_times = np.concatenate((times_HMR, times_CHUM)).astype(np.float32) 
mcgill_times = np.concatenate((times_HGJ, times_CHUS, times_HMR, times_CHUM)).astype(np.float32) 
maastro_times = times_HN1_MAASTRO.astype(np.float32) 
         
#%%
# LOAD CLINICAL COVARIATES
    
# set excel sheet column number for clincial covariates 
if clinical_var:
    # tumor stage, etc
    clinical_to_extract_mcgill = {'Age': 2, 'Sex': 1, 'Site': 3, 'TNM': 8, 'T': 5, 'N': 6, 'Volume':4} 
    clinical_to_extract_maastro = {'Age': 3, 'Sex': 4, 'Site': 1, 'TNM': 10, 'T': 7, 'N': 8, 'Volume':2} 
    
    # load the covariates                                        
    clinical_HGJ, clinical_CHUS, clinical_HMR, clinical_CHUM, clinical_HN1_MAASTRO = \
                                                        load_clinical.load_clinical_covariates(path_to_labels_all, \
                                                            clinical_to_extract_mcgill,clinical_to_extract_maastro)
  
    #train_clinical = np.concatenate((clinical_HGJ, clinical_CHUS)).astype(np.float32) 
    #val_clinical = np.concatenate((clinical_HMR, clinical_CHUM)).astype(np.float32) 
    maastro_clinical = clinical_HN1_MAASTRO.astype(np.float32)      # shape = (137, 6) 
    mcgill_clinical = np.concatenate((clinical_HGJ, clinical_CHUS, clinical_HMR, clinical_CHUM)).astype(np.float32) 

#%%
# SET PATHS TO (MASKED) CTs (paths are used in main scripts to load the data)
    
# set patients to take. Here we are using all patients but if for instance
# only a subset is to be used it could be set here via slicing
patients_HGJ =  np.arange(1,path_to_labels_mcgill['patients_nr'][0] + 1) # HGJ contains 87 patients
patients_CHUS =  np.arange(1,path_to_labels_mcgill['patients_nr'][1] + 1) # CHUS contains 101 patients       
patients_HMR = np.arange(1,path_to_labels_mcgill['patients_nr'][2] + 1) # HMR contains 41 patients
patients_CHUM = np.arange(1,path_to_labels_mcgill['patients_nr'][3] + 1) # CHUM contains 65 patients        
patients_MAASTRO = np.arange(1,path_to_labels_maastro['patients_nr'] + 1)   

# create list with paths to masked cts     
paths_HGJ_masked = [base_path + '/data/preprocessed/McGill/HGJ_imaging/patient' \
                     + str("%03d" % k) + '/MaskedCT/resampled_masked_CT.mha' for k in patients_HGJ]
paths_CHUS_masked  = [base_path + '/data/preprocessed/McGill/CHUS_imaging/patient' \
                     + str("%03d" % k) + '/MaskedCT/resampled_masked_CT.mha' for k in patients_CHUS]
paths_HMR_masked  = [base_path + '/data/preprocessed/McGill/HMR_imaging/patient' \
                     + str("%03d" % k) + '/MaskedCT/resampled_masked_CT.mha' for k in patients_HMR]
paths_CHUM_masked  = [base_path + '/data/preprocessed/McGill/CHUM_imaging/patient' \
                     + str("%03d" % k) + '/MaskedCT/resampled_masked_CT.mha' for k in patients_CHUM]
                                
paths_MAASTRO_masked  = [base_path + '/data/preprocessed/MAASTRO/imaging/patient' \
                        + str("%03d" % k) + '/MaskedCT/resampled_masked_CT.mha' for k in patients_MAASTRO]

          
# np.arrays with paths to masked cts
#train_files_masked = np.array(files_HGJ_masked + files_CHUS_masked)  
#val_files_masked = np.array(files_HMR_masked + files_CHUM_masked)   
mcgill_paths_masked = np.array(paths_HGJ_masked  + paths_CHUS_masked  + paths_HMR_masked + paths_CHUM_masked)                 
maastro_paths_masked = np.array(paths_MAASTRO_masked) 
    
#%%
# SET GPU SETTINGS
eager_mode = False
gpu_limit = False

import tensorflow 
# TF2; disable eager mode could help speeding up or is needed for K.clear_session() to work
if eager_mode == False:
    tensorflow.compat.v1.disable_eager_execution() 
    print('Eager mode disabled.')
if eager_mode:
    tensorflow.compat.v1.enable_eager_execution() 
    print('Eager mode enabled.')
# TF2; limit memory usage to e.g. 16 GB, might be needed if graphical application has to be run in parallel to training
if gpu_limit:
    print('GPU limit active.')
    gpus = tensorflow.config.experimental.list_physical_devices('GPU')
    if gpus:
      try:
        tensorflow.config.experimental.set_virtual_device_configuration(gpus[0], \
                                [tensorflow.config.experimental.VirtualDeviceConfiguration(memory_limit=16000)])
      except RuntimeError as e:
        print(e)

#%%
# SET PLOTTING SETTINGS
import matplotlib
# to run on cluster with no display --> needs to be deactivated to display images
matplotlib.use('agg') 
