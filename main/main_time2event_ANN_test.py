#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 29 09:21:44 2020

@author: Elia.Lombardo

MAIN PROGRAM TO TEST MODELS AND GET CONFIDENCE INTERVALS WITH BOOTSTRAP RESAMPLING FOR TIME2EVENT
"""

import numpy as np
import os
#import tensorflow.keras.backend as K

# import self written modules
import config
if config.phase != 'test':
    raise Exception ('A testing script is being run although in config.py phase is not set to test !' )
if config.task != 'time2event':
    raise Exception ('A time-to-event script is being run although in config.py task is not set to time-to-event!' )
import architecture
import metrics
import nnet_survival
from plot_functions import compute_stats_and_plot_hist, compute_pvalue_and_plot_km
from utils import subdir_paths, get_validation_threshold

# import settings from config.py
from config import task
from config import inputs
from config import cohort
from config import trained_models
from config import bootstrap
from config import bootstrap_resampling
from config import num_neurons_hl
from config import time_span
from config import fixed_interval_width



models_path = config.results_path_time2event +  '/ANN/runs_keras_' + \
                str(config.endpoint) + '/' + trained_models
results_path = config.results_path_time2event +  '/ANN/tests_keras_' + str(config.endpoint) + \
                '/' + trained_models + '_bootstrap/'
os.makedirs(results_path, exist_ok=True)
#%%
# Define the time intervals for the discrete-time survival model
# Since there are fewer events per day at later follow-up times, increasing
# time interval width at later times may improve parameter estimation.

if fixed_interval_width:
	breaks=np.arange(0.,365.*time_span,365//2)
	n_intervals=len(breaks)-1
	timegap = breaks[1:] - breaks[:-1]
else:
	halflife=365.*2
	breaks=-np.log(1-np.arange(0.0,0.96,0.05))*halflife/np.log(2) 
	n_intervals=len(breaks)-1
	timegap = breaks[1:] - breaks[:-1]


#%%
#LOAD DATA
print('\n')
print('Loading the data...')

if cohort == 'maastro':
    clinical = config.maastro_clinical
    labels = config.maastro_labels
    times = config.maastro_times
else:
    raise Exception('Write here other testing cohorts of choice.')
       
#surv_array = nnet_survival.make_surv_array(times, labels, breaks)
#%%
# LOAD BEST MODELS FOR GIVEN CROSS VALIDATION        
           
best_models = []
val_indices = []
# loop over different folds
for fold in subdir_paths(models_path):
    hcis = []
    model_files = [] 
    #print(fold)

    # loop over all subfolders and files of one cross validation fold
    for dirName, subdirList, fileList in os.walk(fold):
        for file in fileList:
            if file[-5:] == '.hdf5':
                model_files.append(file)
                # append all the AUC values
                hcis.append(float(file[-10:-5]))
                
    hcis = np.array(hcis)
    model_files = np.array(model_files)
    
    # find best model by looking at the largest AUC for the given fold
    best_model_of_fold = model_files[np.argmax(hcis)]
    print(fold + '/' + best_model_of_fold)
    path_to_best_model = fold + '/' + best_model_of_fold 
    best_models.append(path_to_best_model) 
    val_indices.append(np.loadtxt(fold + '/val_idx'))   

# load weights of best CV models
models = []
for model_path in best_models:   
    model = architecture.ANN_clinical(len_clinical=clinical.shape[1], \
                                  num_neurons_hl=num_neurons_hl,\
                                  dropout=0, l2=0, task=task, n_intervals=n_intervals) 

    model.load_weights(model_path)   
    models.append(model)
#%%
# TEST MODELS AND USE BOOTSTRAP RESAMPLING TO GET CONFIDENCE INTERVALS

if bootstrap_resampling:
    # bootstrap resampling
    hci_scores = list()
    for sample_counter in range(bootstrap):
        print("==============================================")
        print("====== Current bootstrap sample => %d/%d =======" % (sample_counter+1,bootstrap))
        print("==============================================")
        
        # bootstrap sample indices
        indices = np.random.randint(0, len(clinical), len(clinical))
        #print('Indices of current bootstrap sample: ' + str(indices))
        
        preds = []        
        # loop over different best models
        for fold in range(len(best_models)):
    
            pred = models[fold].predict(clinical[indices], verbose=2)
            
            #print('Predictions: ' + str(pred))
            #print(len(pred))
            #print(len(labels))
            
            preds.append(pred)
            
        # model averaging i.e. averaging the predictions
        averaged_pred = np.mean(preds, axis=0) 
        
        try:    
            # get concordance index at 1yr
            #averaged_ci_1yr = CI(averaged_pred, breaks, times, labels, 1)
            #print('Concordance Index at 1 year: ' + str(ci_1yr))
            # get concordance index at 3yr
            averaged_hci_3yr = metrics.hci(averaged_pred, breaks, times[indices], labels[indices], 3)
            print('Harrel"s concordance Index at 3 years: ' + str(averaged_hci_3yr))
            
            # store bootstrap hci
            hci_scores.append(averaged_hci_3yr) 
        
        # catch zero division error which happens (rarely) in HCI computation when there are no permissible pairs
        except ZeroDivisionError:
            with open(results_path + '/exceptions_' + cohort, "a") as f:
                print('ZeroDivisionError occourred at bootstrap ' + str(sample_counter+1), file=f)
         
    
    
    compute_stats_and_plot_hist(scores=hci_scores, path=results_path, \
                                cohort=cohort, task=task, bootstrap=bootstrap)
    
    # save scores
    np.savetxt(results_path + '/bootstrap_model_averaged_hci_scores_' + cohort, hci_scores)

#%% 
#KAPLAN-MEIER ANALYSIS ON TRUE TEST SET

# compute model averaged concordance index for 'real' testing set

preds = []              
# loop over different best models
for fold in range(len(best_models)):    

    models[fold].summary()
    #print('Metrics in model: ' + str(models[fold].metrics_names))

    pred = models[fold].predict(clinical, batch_size=1, verbose=1)
    
    #print('Predictions: ' + str(pred))
    #print(len(pred))
    #print(len(labels))
    
    preds.append(pred)

# model averaging i.e. averaging the predictions
averaged_pred = np.mean(preds, axis=0) 

# get concordance index at 1yr
#averaged_ci_1yr = CI(averaged_pred, breaks, times, labels, 1)
#print('Concordance Index at 1 year: ' + str(ci_1yr))
# get concordance index at 3yr
averaged_hci_3yr = metrics.hci(averaged_pred, breaks, times, labels, 3)
print('Harrell"s concordance Index at 3 years for real test set: ' + str(averaged_hci_3yr))
with open(results_path + '/real_model_averaged_HCI_3yr_' + cohort, 'w') as f:
    f.write('Model averged concordance Index at 3 years for real test set: \n' + str(averaged_hci_3yr) + '\n')

print('\nLoading validation covariates...')
# load data from validation cv splitting to find threshold by averaging over the folds
train_val_clinical = config.mcgill_clinical
train_val_labels = config.mcgill_labels
    
threshold = get_validation_threshold(data=[], labels=train_val_labels, clinical=train_val_clinical, val_indices=val_indices, \
                                     models=models, n_folds=len(best_models), inputs=inputs, clinical_var=True)
 
print('(Validation) Threshold used for stratification: ' + str(threshold))
with open(results_path + '/validation_threshold', 'w') as f:
    f.write('Threshold from validation: \n' + str(threshold) + '\n')
    
# find high and low risk groups using the threshold
time_averaged_pred = np.mean(averaged_pred, axis=1) #e.g.  shape = (136,) for maastro
high_risk_patient_indices = np.where(time_averaged_pred < threshold)
low_risk_patient_indices = np.where(time_averaged_pred > threshold)
print('Number of high risk patients: ' + str(len(high_risk_patient_indices)))


# for each of the two groups, plot true survival distribution (Kaplan-Meier estimator)
compute_pvalue_and_plot_km(times, labels, high_risk_patient_indices, low_risk_patient_indices, \
                           endpoint=config.endpoint, path=results_path, cohort=cohort) 
