#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 12:59:07 2020

@author: Elia.Lombardo

MAIN PROGRAM TO TRAIN AND VALIDATE ANN IN KERAS USING CROSS VALIDATION
"""
import time
import numpy as np
import os
import gc
from tensorflow.keras.optimizers import SGD, Adam
#from tensorflow.keras.metrics import AUC
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import backend as K
from sklearn.model_selection import StratifiedKFold
#from numpy.random import seed
#seed(1)
#from tensorflow import set_random_seed
#set_random_seed(2)

# import self written modules
import config
if config.phase != 'train':
    raise Exception ('A training script is being run although in config.py phase is not set to train !' )
if config.task != 'classification':
    raise Exception ('A classification script is being run although in config.py task is not set to classification!' )
import architecture
import plot_functions 
import metrics

# import settings from config.py
from config import task
from config import batch_size 
from config import learning_rate 
from config import momentum 
from config import dropout_rate
from config import weight_decay
from config import num_epochs 
from config import n_folds 
from config import shuffle_folds
from config import opt
from config import num_neurons_hl


date_string = time.strftime("%Y-%m-%d-%H:%M")  # time at which run is started. Used to save results
#%%
# LOAD DATA

# set the network's inputs (clinical variables)
train_val_clinical = config.mcgill_clinical
    
# set the labels
train_val_labels = config.mcgill_labels
    
print('The loaded data has the following shape: ' + str(train_val_clinical.shape))
#%%
# TRAIN MODELS

# train model with CV
print('\n')
print('Training model with ' +str(n_folds) + '-fold CV...')

kfold = StratifiedKFold(n_splits=n_folds, shuffle=shuffle_folds)

fold_counter = 1
for train, val in kfold.split(train_val_clinical, train_val_labels):
    print("==============================================")
    print("====== K-Fold Validation step => %d/%d =======" % (fold_counter,n_folds))
    print("==============================================")
    
    train_clinical_cv = train_val_clinical[train]
    train_labels_cv = train_val_labels[train]
    val_clinical_cv = train_val_clinical[val]
    val_labels_cv = train_val_labels[val]
    
    print(train_labels_cv)
    print(val_labels_cv)
        
    model = architecture.ANN_clinical(len_clinical=train_clinical_cv.shape[1], num_neurons_hl=num_neurons_hl, \
                                      dropout=dropout_rate, l2=weight_decay, task=task)

    if opt == 'SGD':
        optimizer = SGD(lr=learning_rate, momentum=momentum)
    if opt == 'Adam':
        optimizer= Adam(lr=learning_rate)
    
    model.compile(loss='binary_crossentropy', \
                  optimizer=optimizer)
                  #metrics=['accuracy', AUC()]) 
                  # using keras AUC metric would be a bit faster than 
                  # custom AUC callback but 
                  # since TF2 I get an error when saving models custom paths like below
                  
    if fold_counter == 1:
        model.summary()
        
    # paths to save results
    results_path = config.results_path_classification +  '/ANN/runs_keras_' + \
                    str(config.endpoint) + '/' + date_string + '/fold_' + str(fold_counter)
    
    if opt == 'SGD':                 
        parameters_path = '/' + date_string + '_lr' + str (learning_rate) + 'momentum' + str(momentum) + \
                            'wd' + str(weight_decay) + 'dropout' + str(int(dropout_rate*100)) + \
                            'batchsize' + str(batch_size) + 'neurons_hl' + str(num_neurons_hl) + \
                            'foldshuffle' + str(shuffle_folds) 
    if opt == 'Adam':                 
        parameters_path = '/' + date_string + '_lr' + str (learning_rate) + \
                            'wd' + str(weight_decay) + 'dropout' + str(int(dropout_rate*100)) + \
                            'batchsize' + str(batch_size) + 'neurons_hl' + str(num_neurons_hl) + \
                            'foldshuffle' + str(shuffle_folds) 
                            
    # create folder to save results
    os.makedirs(results_path, exist_ok=True)
    
    # save training and validation indices for the different folds
    np.savetxt(results_path + '/train_idx', train)
    np.savetxt(results_path + '/val_idx', val)
        
    # save current best model according to AUC using custom callback
    checkpoint_best = metrics.CheckpointAUC(train_data=train_clinical_cv, val_data=val_clinical_cv, \
                                            train_events=train_labels_cv, val_events=val_labels_cv, inputs=config.inputs, \
                                            filepath=results_path + \
                                            '/best_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                            monitor='val_auc', verbose=2,\
                                            save_best_only=True, save_weights_only=True, mode='max')
    # save a model every x epochs
    checkpoint_periodic = metrics.CheckpointAUC(train_data=train_clinical_cv, val_data=val_clinical_cv, \
                                                train_events=train_labels_cv, val_events=val_labels_cv, inputs=config.inputs, \
                                                filepath=results_path + \
                                                '/periodic_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                                monitor='val_auc', verbose=2,\
                                                save_best_only=False, save_weights_only=True, period=1000)

    early_stopping = EarlyStopping(monitor='val_auc', patience=2000, mode='max')

    history=model.fit(train_clinical_cv, train_labels_cv, \
                      validation_data = (val_clinical_cv, val_labels_cv), \
                      batch_size=batch_size, epochs=num_epochs, \
                      callbacks=[checkpoint_best, checkpoint_periodic, early_stopping], \
                      verbose=2)

    # select histories to plot & save
    plot_functions.plot_histories(history=history, results_path=results_path, parameters_path=parameters_path, \
                                  fold_counter=fold_counter, \
                                  grid=True, opt=opt, \
                                  loss=True, auc=True, \
                                  hci_1yr=False, hci_3yr=False)
    
    # clear some (GPU) memory and reset layer, metrics, etc 
    K.clear_session()
    gc.collect()
    
    fold_counter += 1
    