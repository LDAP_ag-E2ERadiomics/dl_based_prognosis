#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 23 09:14:31 2020

@author: Elia.Lombardo

MAIN PROGRAM TO TEST MODELS AND GET CONFIDENCE INTERVALS WITH BOOTSTRAP RESAMPLING
"""

import numpy as np
import os
#import tensorflow.keras.backend as K

# import self written modules
import config
if config.phase != 'test':
    raise Exception ('A testing script is being run although in config.py phase is not set to test !' )
if config.task != 'classification':
    raise Exception ('A classification script is being run although in config.py task is not set to classification!' )
import architecture
from metrics import get_roc_metrics
from plot_functions import compute_stats_and_plot_hist
from utils import subdir_paths

from config import task
from config import cohort
from config import trained_models
from config import bootstrap_resampling
from config import bootstrap
from config import num_neurons_hl


models_path = config.results_path_classification +  '/ANN/runs_keras_' + \
                str(config.endpoint) + '/' + trained_models
results_path = config.results_path_classification +  '/ANN/tests_keras_' + str(config.endpoint) + \
                '/' + trained_models + '_bootstrap/'
os.makedirs(results_path, exist_ok=True)
#%%
#LOAD DATA

print('\n')
print('Loading the data...')
  
if cohort == 'maastro':
    clinical = config.maastro_clinical
    labels = config.maastro_labels
else:
    raise Exception('Write here other testing cohorts of choice.')
        
#%%
# LOAD BEST MODELS FOR GIVEN CROSS VALIDATION        
           
best_models = []
# loop over different folds
for fold in subdir_paths(models_path):
    aucs = []
    model_files = [] 
    #print(fold)

    # loop over all subfolders and files of one cross validation fold
    for dirName, subdirList, fileList in os.walk(fold):
        for file in fileList:
            if file[-5:] == '.hdf5':
                model_files.append(file)
                # append all the AUC values
                aucs.append(float(file[-10:-5]))
                
    aucs = np.array(aucs)
    model_files = np.array(model_files)
    
    # find best model by looking at the largest AUC for the given fold
    best_model_of_fold = model_files[np.argmax(aucs)]
    print(fold + '/' + best_model_of_fold)
    path_to_best_model = fold + '/' + best_model_of_fold 
    best_models.append(path_to_best_model)       
        
# load weights of best models
models = []
for model_path in best_models:   
    model = architecture.ANN_clinical(len_clinical=clinical.shape[1], \
                                  num_neurons_hl=num_neurons_hl,\
                                  dropout=0, l2=0, task=task) 

    model.load_weights(model_path)   
    models.append(model)
#%%
# TEST MODELS AND USE BOOTSTRAP RESAMPLING TO GET CONFIDENCE INTERVALS

if bootstrap_resampling:
    # bootstrap resampling
    auc_scores = list()
    for sample_counter in range(bootstrap):
        print("==============================================")
        print("====== Current bootstrap sample => %d/%d =======" % (sample_counter+1,bootstrap))
        print("==============================================")
        
        # bootstrap sample indices
        indices = np.random.randint(0, len(clinical), len(clinical))
        #print('Indices of current bootstrap sample: ' + str(indices))
        #print(np.shape(clinical))
        
        preds = []        
        # loop over different best models
        for fold in range(len(best_models)):
            
            pred = models[fold].predict(clinical[indices], batch_size=1, verbose=2)
#            models[fold].summary()
#            print(len(pred))
#            print(len(labels))
            
            preds.append(pred)
            
        # model averaging i.e. averaging the predictions
        averaged_pred = np.mean(preds, axis=0) 
        # get averaged roc metrics
        averaged_fpr, averaged_tpr, averaged_thresholds, \
        averaged_roc_auc = get_roc_metrics(labels[indices], averaged_pred)   
        
        # append auc score for current bootstrap replica
        auc_scores.append(averaged_roc_auc) 
        print('ROC AUC: ' + str(averaged_roc_auc))
  
    compute_stats_and_plot_hist(scores=auc_scores, path=results_path, \
                                cohort=cohort, task=task, bootstrap=bootstrap)
    
    # save scores
    np.savetxt(results_path + '/bootstrap_model_averaged_auc_scores_' + cohort, auc_scores)
