#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 09:34:36 2020

@author: Elia.Lombardo

MAIN PROGRAM TO TRAIN AND VALIDATE 3D NETWORK IN KERAS USING CROSS VALIDATION
"""

import numpy as np
import os
import time
import gc
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping
#from tensorflow.keras.metrics import AUC
from tensorflow.keras import backend as K
from sklearn.model_selection import StratifiedKFold

# import self written modules
import config
if config.phase != 'train':
    raise Exception ('A training script is being run although in config.py phase is not set to train !' )
if config.task != 'classification':
    raise Exception ('A classification script is being run although in config.py task is not set to classification!' )
import architecture
import plot_functions 
import metrics
import data_generators
import load_images

# import DKFZ modules for data augmentation
from batchgenerators.transforms.abstract_transforms import Compose
#from batchgenerators.transforms.noise_transforms import GaussianNoiseTransform
from batchgenerators.transforms.spatial_transforms import MirrorTransform
from batchgenerators.transforms.spatial_transforms import SpatialTransform_2
#from batchgenerators.transforms.spatial_transforms import TransposeAxesTransform
from batchgenerators.dataloading.multi_threaded_augmenter import MultiThreadedAugmenter

# parameters
from config import task
from config import dim
from config import channels        # num of input channels of network
from config import batch_size 
from config import crop_size       # size to which image is cropped  (i.e. patch_size)
from config import shift_range         # shift applied during random crop in percentage [0,1] of the original size (256)
from config import patch_center_dist_from_border  # e.g. 76 for shift_range = 0.1 (i.e. cube of 52x52x52 around center is the limit for the center of the 128 crop)
from config import rot_max # in degree [0,90]
from config import deformation_max # maximal deformation [0,1] relative to the patch_size 
from config import prob_rot 
from config import prob_def 
from config import mirroring 
from config import learning_rate 
from config import momentum 
from config import weight_decay
from config import dropout_rate
from config import num_epochs 
from config import n_folds 
from config import shuffle_folds
from config import opt
from config import clinical_var
from config import binary_mask


if shuffle_folds:
    print('Random selection of cv-folds is performed.')
    
date_string = time.strftime("%Y-%m-%d-%H:%M")  # time at which run is started. Used to save results
#%%
#LOAD DATA

print('\n')
print('Loading the data...')
# load CTs and labels for CNN
data, labels = load_images.load_data(config.mcgill_paths_masked, config.mcgill_labels, \
                                     binary=binary_mask, dim=dim)

# load clinical covariates for CNN+Clinical 
if clinical_var:
    clinical = config.mcgill_clinical 

#%%
# TRAIN MODELS
       
# define n-fold cross validation such that all sets contain the same distribution of classes. 
# shuffle_folds=False mantains ordering during fold selection
kfold = StratifiedKFold(n_splits=n_folds, shuffle=shuffle_folds)

fold_counter = 1
for train, val in kfold.split(data, labels):
    print("==============================================")
    print("====== K-Fold Validation step => %d/%d =======" % (fold_counter,n_folds))
    print("==============================================")
    
    
    print('Setting up the DataLoader...')
#    print('Shape of data: ' + str(data.shape))
#    print('Shape of labels: ' + str(labels.shape))
    print('Train indices: ' + str(train))
    print('Val indices: ' + str(val))
    print('Train labels: ' + str(labels[train]))
    print('Val labels: ' + str(labels[val]))
    if clinical_var:
        print('Train clinical: ' + str(clinical[train]))
        print('Val clinical: ' + str(clinical[val]))
    
    if clinical_var == False:
        train_gen = data_generators.TrainDataLoader((data[train],labels[train]), \
                                                    batch_size=batch_size, \
                                                    clinical=clinical_var)
        val_gen_keras = data_generators.ValDataGenerator((data[val],labels[val]), \
                                                         batch_size=1, \
                                                         crop_size=crop_size, \
                                                         shuffle=False, clinical=clinical_var)
    else:
        train_gen = data_generators.TrainDataLoader((data[train],labels[train],clinical[train]), \
                                                    batch_size=batch_size, \
                                                    clinical=clinical_var)
        val_gen_keras = data_generators.ValDataGenerator((data[val],labels[val],clinical[val]), \
                                                         batch_size=1, \
                                                         crop_size=crop_size, \
                                                         shuffle=False, clinical=clinical_var)        
    
    train_batch = next(train_gen)
    
    print('The loaded training data has the following shape: ' + str(train_batch['data'].shape)) # (b, c, depth, height, width)
    print('Number of training samples in fold: ' + str(len(train)))
    print('Number of validation samples in fold: ' + str(len(val)))
    #plot_batch(train_batch, only_first_two=True)


    # AUGMENT DATA
    if dim == 2:
        mirror_transform = MirrorTransform(axes=(0, 1)) # i.e. flipping. Default prob is 0.5 along each axis
                 
        spatial_transform_2D = SpatialTransform_2(patch_size=(crop_size, crop_size), \
                                        patch_center_dist_from_border=patch_center_dist_from_border, \
                                        do_elastic_deform=True, deformation_scale=(0, deformation_max),  \
                                        do_rotation=True, angle_z=(-rot_max*np.pi/180, rot_max*np.pi/180), \
                                        border_mode_data='constant', border_cval_data=np.min(train_batch['data']), order_data=1, \
                                        random_crop=True,  do_scale=False, \
                                        p_el_per_sample=prob_def, p_rot_per_sample=prob_rot)
            
        #transpose_transform  = TransposeAxesTransform(transpose_any_of_these=(0, 1, 2), data_key="data", label_key="seg", p_per_sample=0.75)
        
        # stack transformations
        my_transforms_train = []
        #my_transforms.append(noise_transform)
        if mirroring == True:
            my_transforms_train.append(mirror_transform)
        my_transforms_train.append(spatial_transform_2D)
        #my_transforms.append(transpose_transform)
        all_transforms_train = Compose(my_transforms_train)
        
        
        print('Performing multithreaded augmentations... \n')
        train_gen_multithreaded = MultiThreadedAugmenter(data_loader=train_gen, transform=all_transforms_train,\
                                                         num_processes=4, num_cached_per_queue=2, \
                                                         seeds=None, pin_memory=False)
        
        
        #DEFINE MODEL
        if clinical_var == False:
            model = architecture.CNN_2D(channels=channels, crop_size=crop_size, \
                                        dropout_rate=dropout_rate, task=task)
        else:
            model = architecture.CNN_plus_clinical_2D(channels=channels, crop_size=crop_size, \
                                                      len_clinical=clinical.shape[1], \
                                                      dropout_rate=dropout_rate, task=task)
            
        if opt == 'SGD':
            optimizer = SGD(lr=learning_rate, decay=weight_decay, momentum=momentum)
        if opt == 'Adam':
            optimizer = Adam(lr=learning_rate, decay=weight_decay)
        
        model.compile(loss='binary_crossentropy', \
                      optimizer=optimizer)
        
        if fold_counter == 1:
            model.summary()  
              
    # 3D augmentations and network    
    if dim == 3:
        #noise_transform = GaussianNoiseTransform(noise_variance=(0, 0.01), p_per_sample=0.2)
        
        mirror_transform = MirrorTransform(axes=(0, 1, 2)) # i.e. flipping. Default prob is 0.5 along each axis
              
        spatial_transform_3D = SpatialTransform_2(patch_size=(crop_size, crop_size, crop_size), \
                                        patch_center_dist_from_border=patch_center_dist_from_border, \
                                        do_elastic_deform=True, deformation_scale=(0, deformation_max),  \
                                        do_rotation=True, angle_x=(-rot_max*np.pi/180, rot_max*np.pi/180), angle_y=(-rot_max*np.pi/180, rot_max*np.pi/180), angle_z=(-rot_max*np.pi/180, rot_max*np.pi/180),\
                                        border_mode_data='constant', border_cval_data=np.min(train_batch['data']), order_data=1, \
                                        random_crop=True,  do_scale=False, \
                                        p_el_per_sample=prob_def, p_rot_per_sample=prob_rot)
               
        #transpose_transform  = TransposeAxesTransform(transpose_any_of_these=(0, 1, 2), data_key="data", label_key="seg", p_per_sample=0.75)
        
        # stack transformations
        my_transforms_train = []
        #my_transforms.append(noise_transform)
        if mirroring == True:
            my_transforms_train.append(mirror_transform)
        my_transforms_train.append(spatial_transform_3D)
        #my_transforms.append(transpose_transform)
        all_transforms_train = Compose(my_transforms_train)
        #all_transforms_train = None
        
        
        print('Performing multithreaded augmentations... \n')
        train_gen_multithreaded = MultiThreadedAugmenter(data_loader=train_gen, transform=all_transforms_train,\
                                                         num_processes=4, num_cached_per_queue=2, \
                                                         seeds=None, pin_memory=False)
        
#        # visualize some augmentations
#        for i in range (0,5):
#           plot_functions.plot_batch(train_gen_multithreaded.next(), only_first_two=True)
#        print('Shape of train_gen_multi: ' + str(train_gen_multithreaded.next()['data'].shape))      
        
    
        # DEFINE MODEL
        if clinical_var == False:
            model = architecture.CNN_3D(channels=channels, crop_size=crop_size, \
                                        dropout_rate=dropout_rate, task=task)
        else:
            model = architecture.CNN_plus_clinical_3D(channels=channels, crop_size=crop_size, \
                                                      len_clinical=clinical.shape[1], \
                                                      dropout_rate=dropout_rate, task=task)
        
        if opt == 'SGD':
            optimizer = SGD(lr=learning_rate, decay=weight_decay, momentum=momentum)
        if opt == 'Adam':
            optimizer= Adam(lr=learning_rate, decay=weight_decay)
        
        model.compile(loss='binary_crossentropy', \
                      optimizer=optimizer)
                      #metrics=['accuracy', AUC()]) 
                      # using keras AUC metric would be a bit faster than 
                      # custom AUC callback but 
                      # since TF2 I get an error when saving models custom paths like below
        
        if fold_counter == 1:
            model.summary()
    

    # TRAIN MODEL AND SAVE RESULTS
    
    # paths to save results
    if clinical_var == False:
        results_path = config.results_path_classification +  '/CNN/runs_keras_' + \
                        str(config.endpoint) + '_' + str(dim) + 'D/' + date_string + '/fold_' + str(fold_counter)
    else:
         results_path = config.results_path_classification +  '/CNNplusClinical/runs_keras_' + \
                        str(config.endpoint) + '_' + str(dim) + 'D/' + date_string + '/fold_' + str(fold_counter)
                        
    if opt == 'SGD':                 
        parameters_path = '/' + date_string + '_lr' + str (learning_rate) + 'momentum' + str(momentum) + 'wd' + str(weight_decay) + \
                            'dropout' + str(int(dropout_rate*100)) + 'batchsize' + str(batch_size) + 'mirroring' + str(mirroring) + \
                            'shift' + str(shift_range) + 'rot' + str(rot_max) + 'def' + str(deformation_max) + \
                            'probrot' + str(prob_rot) + 'probdef' + str(prob_def) + 'foldshuffle' + str(shuffle_folds) \
                            + 'clinical' + str(clinical_var) \
                            + 'binary' + str(binary_mask)
    if opt == 'Adam':                 
        parameters_path = '/' + date_string + '_lr' + str (learning_rate) + 'wd' + str(weight_decay) + \
                            'dropout' + str(int(dropout_rate*100)) + 'batchsize' + str(batch_size) + 'mirroring' + str(mirroring) + \
                            'shift' + str(shift_range) + 'rot' + str(rot_max) + 'def' + str(deformation_max) + \
                            'probrot' + str(prob_rot) + 'probdef' + str(prob_def) + 'foldshuffle' + str(shuffle_folds) \
                            + 'clinical' + str(clinical_var) \
                            + 'binary' + str(binary_mask)
                            
    # create folder to save results
    os.makedirs(results_path, exist_ok=True)
    
    # save training and validation indices for the different folds
    np.savetxt(results_path + '/train_idx', train)
    np.savetxt(results_path + '/val_idx', val)
    
    # make augmented data from batchgenerators usable by keras model
    train_gen_keras = data_generators.TorchToKerasGenerator(train_gen_multithreaded, \
                                                            batches_per_epoch=train_gen.__len__(), \
                                                            clinical=clinical_var)
    
    # display some shapes
    if clinical_var == False:
        X, y = train_gen_keras.__getitem__(0)
        print('Shape before training: ' + str(X.shape)) # (b, depth, height, width, c)
        X, y = val_gen_keras.__getitem__(0)
        print('Shape before validation: ' + str(X.shape) + str('\n')) # (b, depth, height, width, c)
        #plot_functions.plot_example(val_gen_keras, sample_nr=0, slice_3d=64, dim=3, clinical=False, prediction=False)

        # save current best model according to AUC using custom callback
        checkpoint_best = metrics.CheckpointAUC(train_data=data[train], val_data=data[val], \
                                                train_events=labels[train], val_events=labels[val], \
                                                clinical_var=clinical_var, \
                                                inputs=config.inputs, crop_size=crop_size, \
                                                filepath=results_path + \
                                                '/best_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                                monitor='val_auc', verbose=2,\
                                                save_best_only=True, save_weights_only=True, mode='max')
        
        # save a model every x epochs 
        checkpoint_periodic = metrics.CheckpointAUC(train_data=data[train], val_data=data[val], \
                                                train_events=labels[train], val_events=labels[val], \
                                                clinical_var=clinical_var, \
                                                inputs=config.inputs, crop_size=crop_size, \
                                                filepath=results_path + \
                                                '/periodic_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                                monitor='val_auc', verbose=2,\
                                                save_best_only=False, save_weights_only=True, period=200)

    else:
        X_z, y = train_gen_keras.__getitem__(0)
        print('Shape of clinical before training: ' + str(X_z[1].shape)) # (b, dim_one_hot_vector)
        X_z, y = val_gen_keras.__getitem__(0)
        print('Shape of clinical before validation: ' + str(X_z[1].shape) + str('\n')) # (b, dim_one_hot_vector)

        # save current best model according to AUC using custom callback
        checkpoint_best = metrics.CheckpointAUC(train_data=data[train], val_data=data[val], \
                                                train_events=labels[train], val_events=labels[val], \
                                                clinical_var=clinical_var, train_clinical=clinical[train], val_clinical=clinical[val], \
                                                inputs=config.inputs, crop_size=crop_size, \
                                                filepath=results_path + \
                                                '/best_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                                monitor='val_auc', verbose=2,\
                                                save_best_only=True, save_weights_only=True, mode='max')
        
        # save a model every x epochs 
        checkpoint_periodic = metrics.CheckpointAUC(train_data=data[train], val_data=data[val], \
                                                train_events=labels[train], val_events=labels[val], \
                                                clinical_var=clinical_var, train_clinical=clinical[train], val_clinical=clinical[val], \
                                                inputs=config.inputs, crop_size=crop_size, \
                                                filepath=results_path + \
                                                '/periodic_model_epoch_{epoch:02d}_loss_{val_loss:.3f}_AUC_{val_auc:.3f}.hdf5', \
                                                monitor='val_auc', verbose=2,\
                                                save_best_only=False, save_weights_only=True, period=200)
           
        
    early_stopping = EarlyStopping(monitor='val_auc', patience=200, mode='max')
    
    history = model.fit(train_gen_keras,
                        epochs           = num_epochs,
                        steps_per_epoch  = train_gen_keras.__len__(),
                        validation_data  = val_gen_keras,
                        validation_steps = val_gen_keras.__len__(),
                        workers = 2, use_multiprocessing=False, # to avoid workers dying (or non deterministic deadlocks in tf)
                        callbacks = [checkpoint_best, checkpoint_periodic, early_stopping], \
                        verbose=2)
    
    
    # select histories to plot & save
    plot_functions.plot_histories(history=history, results_path=results_path, parameters_path=parameters_path, \
                                  fold_counter=fold_counter, \
                                  grid=True, opt=opt, \
                                  loss=True, auc=True, \
                                  hci_1yr=False, hci_3yr=False)

    # kill the workers
    #train_gen_multithreaded._finish() # kill the workers
    
    # clear some (GPU) memory and reset layer, metrics, etc 
    K.clear_session()
    gc.collect()
    
    fold_counter += 1
