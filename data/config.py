#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 09:03:10 2020

@author: Elia.Lombardo

Configuration file for data organisation main scripts 
"""
# path to project folder
base_path = '/project/med5/End2EndRadiomics/Public_Gitlab_Repo/dl_based_prognosis'

# add project auxiliary folder to your Python path to be able to import self written modules
import sys
sys.path.append(base_path + '/auxiliary')

