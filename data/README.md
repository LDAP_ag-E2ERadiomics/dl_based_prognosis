# Data Structure
In the folder `preprocessed` you can find 13 patients from the [Head-Neck-PET-CT cohort on TCIA](https://wiki.cancerimagingarchive.net/display/Public/Head-Neck-PET-CT)
and 6 patients from the [Head-Neck-Radiomics-HN1 cohort on TCIA](https://wiki.cancerimagingarchive.net/display/Public/Head-Neck-Radiomics-HN1)
which were preprocessed (i.e. CTs were masked with the RTSTRUCT, isotropically resampled, etc.) with 
custom in-house data organization scripts. These scripts can be shared upon request.

When using your own preprocessed data, make sure it follows the same folder structure as the shown examples. Alternatively,
you can simply change the paths to the patient's image data in the `dl_based_prognosis/main/config.py` file.
