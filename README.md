# Deep learning based binary classification and time-to-event analysis
Framework to train, validate and test different deep learning models (2D and 3D CNNs, 2D and 3D CNNs+Clinical, ANNs) for 
prediction and prognosis of clinical endpoints. A few preprocessed head & neck cancer data samples from
[The Cancer Imaging Archive](https://www.cancerimagingarchive.net/collections/) are provided to allow trying out the code.

Elia Lombardo\
LMU Munich\
Elia.Lombardo@med.uni-muenchen.de

Evaluated with Python version 3.6.9 and Tensorflow version 2.2.0

## Installation
* Download the repository to a local folder of your preference or clone the repository.
* Enter the directory `dl_based_prognosis`, create a virtual environment (optional) and 
run `pip3 install -r requirements.txt`.
* Open `dl_based_prognosis/main/config.py` and change `base_path` to your local path to the `dl_based_prognosis` folder.

## Usage
* The only file which needs to be modified is `dl_based_prognosis/main/config.py`. Here you can set all the options for your
models, e.g. whether to perform training or testing, whether to perform binary classification or time-to-event, and all the
hyper-parameter settings. 
* After that, in the directory `dl_based_prognosis/main`, run the corresponding script in the terminal. 
For instance `python3 main_classification_CNN_cv.py` to run a cross-validation for a binary clasisfication CNN. 
* The weights of the best models, metrics histories, etc. will be saved under `dl_based_prognosis/results`.

Note: an exception will be thrown if you selected an option in the config file
which does not correpsond to the main script which is run
(e.g. selecting `phase='train'` but running `main_classification_CNN_test.py`). 

## Publication
If you use this code in a scientific publication, please cite our paper: \
[Lombardo, E., Kurz, C., Marschner, S. et al. 
Distant metastasis time to event analysis with CNNs in independent head and neck cancer cohorts. 
Sci Rep 11, 6418 (2021)](https://www.nature.com/articles/s41598-021-85671-y)
