#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 11:07:26 2020

@author: Elia.Lombardo

Useful helper functions
"""
import os # to work with folders etc
import SimpleITK as sitk    # to work with .mha images
from subprocess import Popen, PIPE
from subprocess import run
import numpy as np
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

# import self written modules
import data_generators


def subdir_paths (path):
    " Given a path the function returns only primary subdirectories in a sorted list. "
    return filter(os.path.isdir, [os.path.join(path,f) for f in sorted(os.listdir(path))])


def read_mha(path):
    """
    ​Given a path the function reads the image and returns header as well as the intensity values into a 3d array.
    :param path: String value that represents the path to the .mha file
    :return: image executable and 3D array filled with intensity values
    """
    reader = sitk.ImageFileReader()
    reader.SetFileName(path)
    image = reader.Execute()
    
    # get array with values from sitk image
    image_array = sitk.GetArrayFromImage(image)
    
    return image, image_array
 
    
def write_mha(path, image_array, image=None):
    """
    Given a 3d array and header the function saves a .mha file at the requested path.
    :param image: Executable of image (contains header information, etc.). Highly recommended to provide it
    :param image_array: 3d array filled with intensity values corresponding to a .mha file
    :param path: String value that represents the path to the created .mha file
    :return: Image file corresponding to the input is saved to path
    """
    # get sitk image from array of values
    new_image = sitk.GetImageFromArray(image_array)
#    print('Size of image: ' + str(new_image.GetSize())) # (256, 256, 256)

    # write header info into new image
    if image is not None:
        new_image.SetOrigin(image.GetOrigin())
        new_image.SetSpacing(image.GetSpacing())
        
    # save image
    writer = sitk.ImageFileWriter()
    writer.SetFileName(path)
    writer.Execute(new_image)
    
    
def resample_mha(path, new_spacing, interpolator=sitk.sitkLinear, default_value=-1000):
    """
    Given a path to an sitk image (e.g. .mha file) it resamples the image to a given spacing
    :param path: path to image
    :param new_spacing: spacing to which image is resampled, e.g. [1,1,1] for isotropic resampling
    :param interpolator: which interpolator to use, e.g. sitk.sitkLinear, sitk.sitkNearestNeighbor, sitk.sitkBSpline
    :param default_value: pixel value when a transformed pixel is outside of the image. The default default pixel value is 0.
    :return: resampled image executable
    """
    # load sitk image and create instance of ResampleImageFilter class 
    image = sitk.ReadImage(path)
    resample = sitk.ResampleImageFilter()
    
    # set parameters for resample instance
    resample.SetInterpolator(interpolator)
    resample.SetDefaultPixelValue(default_value)
    resample.SetOutputDirection(image.GetDirection())
    resample.SetOutputOrigin(image.GetOrigin())
#    print('Original origin: ' + str(image.GetOrigin()))
#    print('New origin: ' + str(resample.GetOutputOrigin()))
    resample.SetOutputSpacing(new_spacing)
    
    # compute new size and set parameters for resample instance
    orig_size = np.array(image.GetSize(), dtype=np.int)
    orig_spacing = np.array(image.GetSpacing(), dtype=np.float)
    new_spacing = np.array(new_spacing, dtype=np.float)
    new_size = orig_size*(orig_spacing/new_spacing)
    new_size = np.ceil(new_size).astype(np.int) #  image dimensions are in integers
    new_size = [int(s) for s in new_size]
    resample.SetSize(new_size) 
#    print('Original spacing: ' + str(orig_spacing)) # [1.14453125 1.14453125 3.  ]
#    print('New spacing: ' + str(new_spacing)) # [1. 1. 1.]
#    print('Original size: ' + str(orig_size)) # [512 512 135]
#    print('New size: ' + str(new_size)) # [586, 586, 405]

   # perform the resampling with the params set above
    new_image = resample.Execute(image)
    
    return new_image
    
    
def runcmd(cmd, path_cwd, fn_out=None):
    """
    Runs a command as can be run in shell and prints the output if needed.
    Parameters:
        cmd: str, command as would be written in shell.
        path_cwd: str, current working directory
        fn_out: str, where to write output file
    """

#    print ('\n##########################################################')
#    print ('### SUBPROCESS START\n')

    #process = Popen(cmd, cwd=path_cwd, stdout=PIPE, stderr=PIPE, shell=True)
    
    # result of command line
    result = run(cmd, cwd=path_cwd, stdout=PIPE, stderr=PIPE, shell=True)

  
    if result.returncode == 0:
        #print(result.stdout)
        pass        
    else:
        if result.stderr:
            print('Error in runcmd.py occured: ' + str(result.stderr)) # does not work due do byte to string conversion?

    # write output to file if desired
    if fn_out is not None:
        print ('Writing output to file ' + fn_out + '\n')
        with open (fn_out, 'w') as file_out:
            file_out.write(str(result.stdout))
       
#    print ('### SUBPROCESS END')
#    print ('##########################################################\n')

            
class CustomOneHotEncoder():
    """
    Given an array with labels it translates them into integers and then to one-hot encoded vectors.
    Args:
        values: np.array with e.g. clinical variables of given covariate
        covariate: string, e.g. 'TNM'
    """
    def __init__(self, covariate):      
        self.covariate = covariate
    
    def transform(self, values):

        if self.covariate == 'Site':
            values[np.where((values == 'Larynx') | \
                            (values == 'larynx'))] = 0 
    
            values[np.where((values == 'Nasopharynx') | \
                            (values == '1'))] = 1 

            values[np.where((values == 'Oropharynx') | \
                            (values == '2') | \
                            (values == 'oropharynx'))] = 2 
    
            values[np.where((values == 'Hypopharynx'))] = 3

            values[np.where((values == 'Oral Cavity') | \
                            (values == 'Oral_cavity'))] = 4    
   
            values[np.where((values == 'unknown') | \
                            (values == 'Unknown'))] = 5
    
            # set number of classes for covariate
            nb_classes = 6
            
        if self.covariate == 'Sex':
            values[np.where((values == 'MALE') | \
                            (values == '0') | \
                            (values == 'm') | \
                            (values == 'male') | \
                            (values == 'Male') | \
                            (values == 'M'))] = 0 
    
            values[np.where((values == 'FEMALE') | \
                            (values == '1') | \
                            (values == 'w') | \
                            (values == 'female') | \
                            (values == 'Female') | \
                            (values == 'F'))] = 1 

            # set number of classes for covariate
            nb_classes = 2
            
        if self.covariate == 'TNM':
            # usage of bitwise OR (|) instaed of boolean OR (or) needed
            values[np.where((values == 'I') | \
                            (values == '1.0') | \
                            (values == 'i') | \
                            (values == 'stage I') | \
                            (values == 'Stage I') | \
                            (values == 'Stade I'))] = 0
            
            values[np.where((values == '2.0') | \
                            (values == 'II') | \
                            (values == 'ii') | \
                            (values == 'stage II') | \
                            (values == 'stage IIB') | \
                            (values == 'Stage IIB') | \
                            (values == 'Stage II') | \
                            (values == 'Stade II'))] = 1
    
            values[np.where((values == '3.0') | \
                            (values == 'III') | \
                            (values == 'iii') | \
                            (values == 'stage III') | \
                            (values == 'Stage III') | \
                            (values == 'Stade III'))] = 2

            values[np.where((values == 'IV') | \
                            (values == 'IVA') | \
                            (values == 'IVB') | \
                            (values == '4A') | \
                            (values == '4B') | \
                            (values == 'Stage IVC') | \
                            (values == 'IVa') | \
                            (values == 'IVb') | \
                            (values == 'iva') | \
                            (values == 'ivb') | \
                            (values == 'ivc') | \
                            (values == 'stage IV') | \
                            (values == 'stage IVA') | \
                            (values == 'Stage IVA') | \
                            (values == 'Stade IVA') | \
                            (values == 'stage IVB') | \
                            (values == 'Stage IVB') | \
                            (values == 'Stade IVB') | \
                            (values == 'Stage IV'))] = 3

            values[np.where(values == 'N/A')] = 4
            
            #print(values)
            #print(values.astype(np.int))
            
            # set number of classes for covariate
            nb_classes = 5

        if self.covariate == 'T':
            # usage of bitwise OR (|) instaed of boolean OR (or) needed
            values[np.where((values == 'T1') | \
                            (values == '1.0') | \
                            (values == '1'))] = 0
            
            values[np.where((values == 'T2') | \
                            (values == '2') | \
                            (values == '2.0') | \
                            (values == '2B'))] = 1
    
            values[np.where((values == 'T3') | \
                            (values == '3') | \
                            (values == '3.0') | \
                            (values == 'T3 (2)'))] = 2

            values[np.where((values == 'T4') | \
                            (values == 'T4a') | \
                            (values == 'T4b') | \
                            (values == '4') | \
                            (values == '4.0') | \
                            (values == '4A') | \
                            (values == '4B') | \
                            (values == '4a') | \
                            (values == '4b'))] = 3

            values[np.where(values == 'Tx')] = 4
            
            #print(values)
            #print(values.astype(np.int))
            
            # set number of classes for covariate
            nb_classes = 5
                          
        if self.covariate == 'N':
            # usage of bitwise OR (|) instaed of boolean OR (or) needed
            values[np.where((values == 'N0') | \
                            (values == '0.0') | \
                            (values == '0'))] = 0
            
            values[np.where((values == 'N1') | \
                            (values == '1.0') | \
                            (values == '1'))] = 1
    
            values[np.where((values == 'N2') | \
                            (values == '2') | \
                            (values == '2.0') | \
                            (values == '2A') | \
                            (values == '2B') | \
                            (values == '2C') | \
                            (values == '2a') | \
                            (values == '2b') | \
                            (values == '2c') | \
                            (values == 'N2a') | \
                            (values == 'N2b') | \
                            (values == 'N2c'))] = 2

            values[np.where((values == 'N3') | \
                            (values == 'N3b') | \
                            (values == '3A') | \
                            (values == '3B') | \
                            (values == '3.0') | \
                            (values == '3'))] = 3

            values[np.where(values == 'NX')] = 4
            
            #print(values)
            #print(values.astype(np.int))
            
            # set number of classes for covariate
            nb_classes = 5
                   
        # use np.eye to onehot encode integers
        onehot_values = np.eye(nb_classes)[values.astype(np.int)]
        #print(onehot_values)
                
        return onehot_values
    
    
def output_clinical_var(sheet_cohort, name_cohort, patients_nr, clinical_cohort):
    """
    Given cohort specific information, output np.matrix with clinical variables
    Args:
        sheet_cohort: excel sheet as returned by xrld sheet_by_index module
        name_cohort: string, name for cohort
        patients_nt: int, toral number of patients for that cohort
        clinical_cohort: dict, with names and integers corresponding to column in excel file for desiderd covariate
    """
    
    for covariate in clinical_cohort:              
        print('Extracting ' + str(name_cohort) + ' clinical variables for: ' + \
              str(sheet_cohort.col_values(clinical_cohort[covariate])[0]))
        
        if covariate == 'Age':
            # rescale all ages with 100 to have values between 0 and 1 (all aptients have less than 100 years)
            age_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])/100.0
            age_cohort = np.reshape(age_cohort, (-1, 1)) # shape (x,1) is need to write into matrix

        if covariate == 'Sex':
            # convert all strings to integers and then to one-hot vectors
            custom_onehot_encoder = CustomOneHotEncoder(covariate=covariate)
    
            sex_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])
            sex_cohort = custom_onehot_encoder.transform(sex_cohort)

        if covariate == 'Site':
            # convert all strings to integers and then to one-hot vectors
            custom_onehot_encoder = CustomOneHotEncoder(covariate=covariate)
    
            site_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])
            site_cohort = custom_onehot_encoder.transform(site_cohort)
            
        if covariate == 'TNM':
            # convert all strings to integers and then to one-hot vectors
            custom_onehot_encoder = CustomOneHotEncoder(covariate=covariate)
    
            tnm_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])
            tnm_cohort = custom_onehot_encoder.transform(tnm_cohort)

        if covariate == 'T':
            # convert all strings to integers and then to one-hot vectors
            custom_onehot_encoder = CustomOneHotEncoder(covariate=covariate)
    
            t_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])
            t_cohort = custom_onehot_encoder.transform(t_cohort)

        if covariate == 'N':
            # convert all strings to integers and then to one-hot vectors
            custom_onehot_encoder = CustomOneHotEncoder(covariate=covariate)
    
            n_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])
            n_cohort = custom_onehot_encoder.transform(n_cohort) 
            #print(n_cohort)
 
        if covariate == 'Volume':
            # rescale all volumes with 100 to have values between 0 and 1.5 (more or less, few patients have 100 < ccm GTV)
            #print(np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1]))
            volume_cohort = np.array(sheet_cohort.col_values(clinical_cohort[covariate])[1:patients_nr+1])/100.0
            volume_cohort = np.reshape(volume_cohort, (-1, 1)) # shape (x,1) is need to write into matrix 
            #print(volume_cohort)
            
    
    clinical_cohort = np.matrix(np.empty([patients_nr, \
                                            age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+\
                                            tnm_cohort.shape[1]+ \
                                            t_cohort.shape[1]+n_cohort.shape[1]+ \
                                            volume_cohort.shape[1]]))
    
    #print(clinical_cohort)
    
    # write all covariates of one patient in a single row for every patient
    clinical_cohort[:,0] = age_cohort # columns have to be manually selected (unelegant)
    clinical_cohort[:,age_cohort.shape[1]:age_cohort.shape[1]+sex_cohort.shape[1]] = sex_cohort
    clinical_cohort[:,age_cohort.shape[1]+sex_cohort.shape[1]: \
                    age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]] = site_cohort
    clinical_cohort[:,age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]: \
                    age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]] = tnm_cohort
    clinical_cohort[:,age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]: \
                    age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]+t_cohort.shape[1]] = t_cohort
    clinical_cohort[:,age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]+t_cohort.shape[1]: \
                    age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]+t_cohort.shape[1]+n_cohort.shape[1]] = n_cohort
    clinical_cohort[:,age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]+t_cohort.shape[1]+n_cohort.shape[1]: \
                    age_cohort.shape[1]+sex_cohort.shape[1]+site_cohort.shape[1]+tnm_cohort.shape[1]+t_cohort.shape[1]+n_cohort.shape[1]+volume_cohort.shape[1]] = volume_cohort    

    #print(clinical_cohort[0])
    return clinical_cohort


def compute_stats(scores, path, bootstrap=None, alpha=17):
    " Output mean, median and confidence (83% by default) given the scores obtained via bootstrap resampling."
    
    # write results in txt
    if bootstrap is not None:
        with open(path + str(bootstrap) + '_stats', "a") as f:
            print('================================================================', file=f)
            print('50th percentile (median) = %.3f' % np.median(scores), file=f)
            
            # calculate 95% confidence intervals if alpha=5 (100 - alpha)
            # calculate lower percentile (e.g. 2.5)
            lower_p = alpha / 2.0
            # retrieve observation at lower percentile
            lower = max(0.0, np.percentile(scores, lower_p))
            print('%.1fth percentile = %.3f' % (lower_p, lower), file=f)
            # calculate upper percentile (e.g. 97.5)
            upper_p = (100 - alpha) + (alpha / 2.0)
            # retrieve observation at upper percentile
            upper = min(1.0, np.percentile(scores, upper_p))
            print('%.1fth percentile = %.3f' % (upper_p, upper), file=f)
            
            print('Mean: ' + str(round(np.mean(scores), 3)), file=f)
            print('1.4*std: ' + str(round(1.4*np.std(scores), 3)), file=f)
            print('2*std: ' + str(round(2*np.std(scores), 3)), file=f)
            
        return np.median(scores), lower, upper
    else:
        with open(path, "a") as f:
            print('================================================================', file=f)
            print('50th percentile (median) = %.3f' % np.median(scores), file=f)
            
            # calculate 95% confidence intervals if alpha=5 (100 - alpha)
            # calculate lower percentile (e.g. 2.5)
            lower_p = alpha / 2.0
            # retrieve observation at lower percentile
            lower = max(0.0, np.percentile(scores, lower_p))
            print('%.1fth percentile = %.3f' % (lower_p, lower), file=f)
            # calculate upper percentile (e.g. 97.5)
            upper_p = (100 - alpha) + (alpha / 2.0)
            # retrieve observation at upper percentile
            upper = min(1.0, np.percentile(scores, upper_p))
            print('%.1fth percentile = %.3f' % (upper_p, upper), file=f)
            
            print('Mean: ' + str(np.mean(scores)), file=f)
            print('1.4*std: ' + str(1.4*np.std(scores)), file=f)
            print('2*std: ' + str(2*np.std(scores)), file=f) 


def get_validation_threshold(data, labels, clinical, val_indices, models, n_folds, \
                             inputs, clinical_var, crop_size=128):
    " Given data and models from cross validation, compute threshold (later) used for test patient stratification."
    
    print('Getting threshold for Kaplan-Meier from validation...')    
    # convert indices to array of integers
    #print('Val indices: ' + str(val_indices))
    val_indices = np.array(val_indices)
    for fold in range (0,len(val_indices)):
        val_indices[fold] = val_indices[fold].astype(np.int)
    
    threshold_with_event = []
    threshold_without_event = []
    # loop over best folds and compute the predictions using val data
    for fold in range(n_folds):
        
#        print(val_indices[fold])
#        print(np.shape(val_indices))
        
        if inputs == 'imaging':  
            # as no labels are needed put array with zeros as placeholder for the data generator
            if clinical_var == False:
                test_gen_sample = data_generators.ValDataGenerator((data[val_indices[fold]], labels[val_indices[fold]]), \
                                                                   batch_size=1, \
                                                                   crop_size=crop_size, \
                                                                   shuffle=False, prediction=True, clinical=clinical_var)    
            else:
                test_gen_sample = data_generators.ValDataGenerator((data[val_indices[fold]], labels[val_indices[fold]], clinical[val_indices[fold]]), \
                                                                   batch_size=1, \
                                                                   crop_size=crop_size, \
                                                                   shuffle=False, prediction=True, clinical=clinical_var) 

            pred = models[fold].predict(test_gen_sample, workers=12, \
                                         use_multiprocessing=False, verbose=1)
            
            threshold_with_event.append(np.mean(pred[np.where(labels[val_indices[fold]] == 1)]))
            threshold_without_event.append(np.mean(pred[np.where(labels[val_indices[fold]] == 0)]))
                  
        if inputs == 'clinical_variables':

            pred = models[fold].predict(clinical[val_indices[fold]], batch_size=1, verbose=1)
            
            threshold_with_event.append(np.mean(pred[np.where(labels[val_indices[fold]] == 1)]))
            threshold_without_event.append(np.mean(pred[np.where(labels[val_indices[fold]] == 0)]))
            
    # find final threshold by averaging over the three folds and then averaging with/without event thresholds
    averaged_threshold = (np.mean(threshold_with_event) + np.mean(threshold_without_event))/2

    return averaged_threshold 

def compute_cohort_stats(clinical_cohort, path_results):
    "Compute statistics for cohort given the clinical data."
    
    # convert to np.array
    clinical_cohort = np.array(clinical_cohort)
    nr_patients = len(clinical_cohort)
    
    # write results in txt
    with open(path_results + 'cohort_stats.txt', "a") as f:
        print('================================================================', file=f)
        print('Median Age = %.2f years' % np.median(100*clinical_cohort[:,0], axis=0), file=f)
        print('Percentage of Males = %.d' % (100*np.sum(clinical_cohort[:,1], axis=0)/nr_patients), file=f)
        print('Median GTVp+GTVn = %.2f ccm' % np.median(100*clinical_cohort[:,-1], axis=0), file=f)
        print('Percentage of Larynx = %.d ' % (100*np.sum(clinical_cohort[:,3], axis=0)/nr_patients), file=f)
        print('Percentage of Nasopharynx = %.d ' % (100*np.sum(clinical_cohort[:,4], axis=0)/nr_patients), file=f)
        print('Percentage of Oropharynx = %.d ' % (100*np.sum(clinical_cohort[:,5], axis=0)/nr_patients), file=f)
        print('Percentage of Hypopharynx = %.d ' % (100*np.sum(clinical_cohort[:,6], axis=0)/nr_patients), file=f)
        print('Percentage of Oral Cavity = %.d ' % (100*np.sum(clinical_cohort[:,7], axis=0)/nr_patients), file=f)
        print('Percentage of Unknown Sites = %.d ' % (100*np.sum(clinical_cohort[:,8], axis=0)/nr_patients), file=f)
        print('Percentage of Stage I = %.d ' % (100*np.sum(clinical_cohort[:,9], axis=0)/nr_patients), file=f)
        print('Percentage of Stage II = %.d ' % (100*np.sum(clinical_cohort[:,10], axis=0)/nr_patients), file=f)
        print('Percentage of Stage III = %.d ' % (100*np.sum(clinical_cohort[:,11], axis=0)/nr_patients), file=f)
        print('Percentage of Stage IV = %.d ' % (100*np.sum(clinical_cohort[:,12], axis=0)/nr_patients), file=f)
    