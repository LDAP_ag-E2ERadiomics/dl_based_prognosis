#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 15:43:04 2020

@author: Elia.Lombardo

Functions to load labels, times, clinical covariates
"""

import xlrd     # to read excel file
import numpy as np
from utils import output_clinical_var

#%%
def load_labels(path_to_labels, label_mcgill, label_maastro): 
    """
    Args:
        path_to_labels: list of dictionaries containing name of cohort, path to its labels and total nr of patients
        label_cohort: label number in the excel sheet
    """
    
    for path in path_to_labels:
        
        # extract all labels from McGill study
        if path['name'] == 'mcgill':
            wb = xlrd.open_workbook(path['path'])
            sheet_HGJ = wb.sheet_by_index(0) 
            sheet_CHUS = wb.sheet_by_index(1)
            sheet_HMR = wb.sheet_by_index(2) 
            sheet_CHUM = wb.sheet_by_index(3) 
            
            label_nr = label_mcgill
            print('Extracting ' + str(path['name']) + ' labels for: ' + str(sheet_HGJ.col_values(label_nr)[0]))
            labels_HGJ =  sheet_HGJ.col_values(label_nr)[1:path['patients_nr'][0]+1]            
            labels_CHUS = sheet_CHUS.col_values(label_nr)[1:path['patients_nr'][1]+1]             
            labels_HMR =  sheet_HMR.col_values(label_nr)[1:path['patients_nr'][2]+1]            
            labels_CHUM = sheet_CHUM.col_values(label_nr)[1:path['patients_nr'][3]+1]
        
        # extract all labels from MAASTRO study
        if path['name'] == 'maastro':
            wb = xlrd.open_workbook(path['path'])
            sheet_HN1_MAASTRO = wb.sheet_by_index(0)
            
            label_nr = label_maastro
            print('Extracting ' + str(path['name']) + ' labels for: ' + str(sheet_HN1_MAASTRO.col_values(label_nr)[0]))
            labels_HN1_MAASTRO =  sheet_HN1_MAASTRO.col_values(label_nr)[1:path['patients_nr']+1]

    
    return np.array(labels_HGJ), np.array(labels_CHUS), np.array(labels_HMR), np.array(labels_CHUM), \
            np.array(labels_HN1_MAASTRO)
            
            
            
def load_times(path_to_labels, time_mcgill, time_maastro): 
    """
    Args:
        path_to_labels: list of dictionaries containing name of cohort, path to its clinical data and number of patients in it
        time_cohort: column number of endpoint time2event in the excel sheet
    """
    
    for path in path_to_labels:
        
        # extract all labels from McGill study
        if path['name'] == 'mcgill':
            wb = xlrd.open_workbook(path['path'])
            sheet_HGJ = wb.sheet_by_index(0) 
            sheet_CHUS = wb.sheet_by_index(1)
            sheet_HMR = wb.sheet_by_index(2) 
            sheet_CHUM = wb.sheet_by_index(3) 
 
            print('Extracting ' + str(path['name']) + ' times for: ' + str(sheet_HGJ.col_values(time_mcgill)[0]))
            times_HGJ = sheet_HGJ.col_values(time_mcgill)[1:path['patients_nr'][0]+1]
            
            times_CHUS = sheet_CHUS.col_values(time_mcgill)[1:path['patients_nr'][1]+1] 
            
            times_HMR =  sheet_HMR.col_values(time_mcgill)[1:path['patients_nr'][2]+1]
            
            times_CHUM = sheet_CHUM.col_values(time_mcgill)[1:path['patients_nr'][3]+1]
            
        # extract all labels from MAASTRO study
        if path['name'] == 'maastro':
            wb = xlrd.open_workbook(path['path'])
            sheet_HN1_MAASTRO = wb.sheet_by_index(0)
 
            print('Extracting ' + str(path['name']) + ' times for: ' + str(sheet_HN1_MAASTRO.col_values(time_maastro)[0]))
            times_HN1_MAASTRO =  sheet_HN1_MAASTRO.col_values(time_maastro)[1:path['patients_nr']+1]

    return np.array(times_HGJ), np.array(times_CHUS), np.array(times_HMR), np.array(times_CHUM), \
            np.array(times_HN1_MAASTRO)

def load_clinical_covariates(path_to_labels, clinical_mcgill, clinical_maastro): 
    """
    Args:
        path_to_labels: list of dictionaries containing name of cohort and path to its clinical variables list and patient number
        clinical_cohort: dictionary with covariates numbers in the excel sheet
    """
    
    for path in path_to_labels:
        
        # extract all labels from McGill study
        if path['name'] == 'mcgill':
            wb = xlrd.open_workbook(path['path'])
            sheet_HGJ = wb.sheet_by_index(0) 
            sheet_CHUS = wb.sheet_by_index(1)
            sheet_HMR = wb.sheet_by_index(2) 
            sheet_CHUM = wb.sheet_by_index(3) 
            
            # using self written function output_clinical_var to output covariates
            clinical_HGJ = output_clinical_var(sheet_cohort=sheet_HGJ, name_cohort=path['name'], \
                                               patients_nr=path['patients_nr'][0], clinical_cohort=clinical_mcgill)
            clinical_CHUS = output_clinical_var(sheet_cohort=sheet_CHUS, name_cohort=path['name'], \
                                               patients_nr=path['patients_nr'][1], clinical_cohort=clinical_mcgill)
            clinical_HMR = output_clinical_var(sheet_cohort=sheet_HMR, name_cohort=path['name'], \
                                               patients_nr=path['patients_nr'][2], clinical_cohort=clinical_mcgill)
            clinical_CHUM = output_clinical_var(sheet_cohort=sheet_CHUM, name_cohort=path['name'], \
                                               patients_nr=path['patients_nr'][3], clinical_cohort=clinical_mcgill)            

        # extract all labels from MAASTRO study
        if path['name'] == 'maastro':
            wb = xlrd.open_workbook(path['path'])
            sheet_HN1_MAASTRO = wb.sheet_by_index(0)
            
            clinical_HN1_MAASTRO = output_clinical_var(sheet_cohort=sheet_HN1_MAASTRO, name_cohort=path['name'], \
                                                       patients_nr=path['patients_nr'], clinical_cohort=clinical_maastro)
            

    return clinical_HGJ, clinical_CHUS, clinical_HMR, clinical_CHUM, \
            clinical_HN1_MAASTRO
            
