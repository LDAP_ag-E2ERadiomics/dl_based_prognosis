#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 17:31:02 2020

@author: Elia.Lombardo
"""
import matplotlib.pyplot as plt
import numpy as np
from lifelines import KaplanMeierFitter
from lifelines.statistics import logrank_test


    
def plot_batch(batch, extra_seg=False, only_first_two=False):
    """
    Plot images within batch, regardless if they are single or double channel, 2D or 3D.
    Args:
        extra_seg: if True the segmentation is treated in a special way during the augmentations. The channel size will always be 1 in this case.
        only_first_two: if True only the first two examples within the batch will be plotted (leads to better visualization if batch_size is quite big).
    """
    
    batch_size = batch['data'].shape[0]
    num_ch = batch['data'].shape[1]
    dim = batch['data'].ndim
    
    # 2D case
    if dim == 4:
        if only_first_two == False:
            # extra dictionary entry for the segmentation
            if extra_seg == True:
                if num_ch == 1:
                    fig1, axes1 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(batch_size)):
                        ax.imshow(batch['seg'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch')
                    plt.show()
                                
                else:
                    print('Unexpected number of channels for extra_seg=True.')
                
            # no extra dictionary entry for the masked CT
            if extra_seg == False:
                if num_ch == 1:
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig2.suptitle('Images within a batch')
                    plt.show()
                                
                elif num_ch == 2:
                    fig1, axes1 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 1, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch')
                    plt.show() 
    
                elif num_ch == 3:
                    fig1, axes1 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 1, :, :], cmap="gray")
                    fig2.suptitle('Cropped cts within a batch')
                    plt.show() 
    
                    fig3, axes3 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes3.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 2, :, :], cmap="gray")
                    fig3.suptitle('Full cts within a batch')
                    plt.show() 
                    
                else:
                    print('Unexpected number of channels (=' + str(num_ch) + ') for extra_seg=False.')
        
        if only_first_two == True:
            # extra dictionary entry for the segmentation
            if extra_seg == True:
                if num_ch == 1:
                    fig1, axes1 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(2)):
                        ax.imshow(batch['seg'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch')
                    plt.show()
                                
                else:
                    print('Unexpected number of channels for extra_seg=True.')
                
            # no extra dictionary entry for the masked CT
            if extra_seg == False:
                if num_ch == 1:
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig2.suptitle('Images within a batch')
                    plt.show()
                                
                elif num_ch == 2:
                    fig1, axes1 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 1, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch')
                    plt.show() 
                    
                elif num_ch == 3:
                    fig1, axes1 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 1, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch')
                    plt.show()            
    
                    fig3, axes3 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes3.flat, range(2)):
                        ax.imshow(batch['data'][i, 2, :, :], cmap="gray")
                    fig3.suptitle('Cts within a batch')
                    plt.show()  
                    
                else:
                    print('Unexpected number of channels (=' + str(num_ch) + ') for extra_seg=False.') 

     
    # 3D case               
    if dim == 5:
        if only_first_two == False:
            # extra dictionary entry for the segmentation
            if extra_seg == True:
                if num_ch == 1:
                    fig1, axes1 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(batch_size)):
                        ax.imshow(batch['seg'][i, 0, batch['seg'].shape[2]//2, :, :], cmap="gray") # take the central slice in axial dimension
                    fig1.suptitle('Segmentations within a batch (central axial slice)')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch (central axial slice)')
                    plt.show()
                                
                else:
                    print('Unexpected number of channels for extra_seg=True.')
                
            # no extra dictionary entry for the masked CT
            if extra_seg == False:
                if num_ch == 1:
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Images within a batch (central axial slice)')
                    plt.show()
                                
                elif num_ch == 2:
                    fig1, axes1 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch (central axial slice)')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, batch_size, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(batch_size)):
                        ax.imshow(batch['data'][i, 1, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch (central axial slice)')
                    plt.show() 
                    
                else:
                    print('Unexpected number of channels (=' + str(num_ch) + ') for extra_seg=False.')
        
        if only_first_two == True:
            # extra dictionary entry for the segmentation
            if extra_seg == True:
                if num_ch == 1:
                    fig1, axes1 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(2)):
                        ax.imshow(batch['seg'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch (central axial slice)')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch (central axial slice)')
                    plt.show()
                                
                else:
                    print('Unexpected number of channels for extra_seg=True.')
                
            # no extra dictionary entry for the masked CT
            if extra_seg == False:
                if num_ch == 1:
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Images within a batch (central axial slice)')
                    plt.show()
                                
                elif num_ch == 2:
                    fig1, axes1 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes1.flat, range(2)):
                        ax.imshow(batch['data'][i, 0, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig1.suptitle('Segmentations within a batch (central axial slice)')
                    plt.show()
                    
                    fig2, axes2 = plt.subplots(1, 2, figsize=(12,7))
                    for ax, i in zip(axes2.flat, range(2)):
                        ax.imshow(batch['data'][i, 1, batch['data'].shape[2]//2, :, :], cmap="gray")
                    fig2.suptitle('Cts within a batch (central axial slice)')
                    plt.show() 
                    
                else:
                    print('Unexpected number of channels (=' + str(num_ch) + ') for extra_seg=False.')
                    
def plot_example(data_generator, sample_nr, slice_3d, dim=2, clinical=False, prediction=True):
    """
    Plot image from a keras data generator.
    Args:
        data_generator: keras data generator
        sample_nr: sample instance within generator
        slice_3d: if the image is 3D, select this slice to be plotted
        dim: whether image is 2D or 3D
        clinical: True if clinical covariates are included in generator
        prediction: True if labels are present in generator
    """
    
    if prediction == True:
        if dim ==2:
            plt.figure(figsize=(5,5))
            plt.imshow(data_generator.__getitem__(sample_nr)[0,:,:,0], cmap="gray")
            plt.title('Slice with largest GTV of ' + str(sample_nr) + '. sample')
            plt.show()
            #plt.close()
            
        if dim == 3:
            if clinical == False:
                plt.figure(figsize=(5,5))
                plt.imshow(data_generator.__getitem__(sample_nr)[0,slice_3d,:,:,0], cmap="gray")
                plt.title('Central axial slice of ' + str(sample_nr) + '. sample')
                #plt.title('Central axial slice of CRO patient')
                plt.show()
                #plt.close() 
            else:
                plt.figure(figsize=(5,5))
                plt.imshow(data_generator.__getitem__(sample_nr)[0][0,slice_3d,:,:,0], cmap="gray")
                plt.title('Central axial slice of ' + str(sample_nr) + '. sample')
                plt.show()
                #plt.close() 
    else:
        if dim ==2:
            plt.figure(figsize=(5,5))
            plt.imshow(data_generator.__getitem__(sample_nr)[0][0,:,:,0], cmap="gray")
            plt.title('Slice with largest GTV of ' + str(sample_nr) + '. sample')
            plt.show()
            #plt.close()
            
        if dim == 3:
            if clinical == False:
                plt.figure(figsize=(5,5))
                plt.imshow(data_generator.__getitem__(sample_nr)[0][0,slice_3d,:,:,0], cmap="gray")
                plt.title('Central axial slice of ' + str(sample_nr) + '. sample')
                plt.show()
                #plt.close() 
            else:
                plt.figure(figsize=(5,5))
                plt.imshow(data_generator.__getitem__(sample_nr)[0][0][0,slice_3d,:,:,0], cmap="gray")
                plt.title('Central axial slice of ' + str(sample_nr) + '. sample')
                plt.show()
                #plt.close() 

def plot_histories(history, results_path, parameters_path, fold_counter=None, \
                   grid=True, opt='Adam', \
                   loss=True, auc=False, \
                   hci_1yr=False, hci_3yr=False):
    """
    Args:
        history: history object as returned by keras training
        results_path: path where to save results
        parameters_path: parameters conatined in names of result files
        fold_conter: only for cross validation
        grid: grid option of matplotlib
        opt_SGD: True if stochastic gradient descend is used for training
        opt_Adam: True if adaptive moment estimation is used for training
        loss: plots loss
        auc: plot area under the receiver operating characzerisric curve
        hci_xyr: plots concordance index at x year
    """
    # plot training & validation loss values
    if loss:
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        if fold_counter is not None:
            plt.title('Model ' + str(fold_counter) + ' Loss')
        else:
            plt.title('Model Loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.grid(grid)
        # save figure 
        if opt == 'SGD':
            path_figure = results_path + parameters_path + '_SGD_LOSS.png'
        if opt == 'Adam':
            path_figure = results_path + parameters_path + '_Adam_LOSS.png'
                            
        plt.savefig(path_figure)
        #plt.show()
        plt.close()

    # summarize history for auc
    if auc:
        plt.plot(history.history['auc'])
        plt.plot(history.history['val_auc'])
        if fold_counter is not None:
            plt.title('Model ' + str(fold_counter) + ' ROC AUC')
        else:
            plt.title('Model ROC AUC')
        plt.ylabel('ROC AUC')
        plt.xlabel('Epoch')
        plt.legend(['train', 'val'], loc='lower right')
        plt.grid(grid)
        # save figure 
        if opt == 'SGD':
            path_figure = results_path + parameters_path + '_SGD_ROC_AUC.png'
        if opt == 'Adam':
            path_figure = results_path + parameters_path + '_Adam_ROC_AUC.png'                    
                            
        plt.savefig(path_figure)
        #plt.show()
        plt.close()
    
    
    # summarize history for ci at 1 year
    if hci_1yr:
        plt.plot(history.history['hci_1yr'])
        plt.plot(history.history['val_hci_1yr'])
        if fold_counter is not None:
            plt.title('Model ' + str(fold_counter) + ' Concordance Index at 1 Year')
        else:
            plt.title('Model Concordance Index at 1 Year')            
        plt.ylabel('HCI')
        plt.xlabel('Epoch')
        plt.legend(['train', 'val'], loc='lower right')
        plt.grid(grid)
        # save figure 
        if opt == 'SGD':
            path_figure = results_path + parameters_path + '_SGD_HCI_1yr.png'
        if opt == 'Adam':
            path_figure = results_path + parameters_path + '_Adam_HCI_1yr.png'                    
                            
        plt.savefig(path_figure)
        #plt.show()
        plt.close()
    
    # summarize history for ci at 3 years
    if hci_3yr:
        plt.plot(history.history['hci_3yr'])
        plt.plot(history.history['val_hci_3yr'])
        if fold_counter is not None:
            plt.title('Model ' + str(fold_counter) + ' Concordance Index at 3 Years')
        else:
            plt.title('Model Concordance Index at 3 Years') 
        plt.ylabel('HCI')
        plt.xlabel('Epoch')
        plt.legend(['train', 'val'], loc='lower right')
        plt.grid(grid)
        # save figure 
        if opt == 'SGD':
            path_figure = results_path + parameters_path + '_SGD_HCI_3yr.png'
        if opt == 'Adam':
            path_figure = results_path + parameters_path + '_Adam_HCI_3yr.png'                    
                            
        plt.savefig(path_figure)
        #plt.show()
        plt.close()
        
def plot_roc_auc(fpr, tpr, roc_auc, results_path, cohort, show=False):
    plt.figure(figsize=(7,5))
    plt.plot(fpr, tpr, 'b-', label = 'ROC AUC = %0.2f' %roc_auc)
    plt.plot([0,1], [0,1], 'r--', label = 'No skill ROC AUC')
    plt.legend(loc='lower right')
    plt.ylabel('True Positive Rate (TPR)')
    plt.xlabel('False Positive Rate (FPR)')
    #plt.title('Model Averaged ROC-AUC')
    
    # save figure 
    path_figure = results_path + '/ROC_AUC_' + cohort + '.png'
    
    plt.savefig(path_figure)
    if show:
        plt.show()
    plt.close()
    
def plot_pr_auc(labels, recall, precision, pr_auc, results_path, cohort, show=False):
    plt.figure(figsize=(7,5))
    plt.plot(recall, precision, marker='.', label='PR AUC = %0.2f' %pr_auc)
    no_skill = len(labels[labels == 1])/len(labels)
    plt.plot([0,1], [no_skill, no_skill], 'r--', label='No skill PR AUC = %0.2f' %no_skill)
    plt.legend(loc='center left')
    plt.ylabel('Precision (PPV)')
    plt.xlabel('Recall (TPR)')
    plt.ylim(-0.05, 1.05) 
    #plt.title('Model Averaged PR-AUC')
    
    # save figure 
    path_figure = results_path + '/PR_AUC_' + cohort + '.png'
    
    plt.savefig(path_figure)
    if show:
        plt.show()
    plt.close()
    
def plot_preds_distr(pred, labels, results_path, cohort, show=False):
    """Plot histogram of outcome prediction distribution""" 
    fig, ax = plt.subplots(figsize=(7,5))
    
    N0, bins0, patches0 = ax.hist(pred[np.where(labels == 0)], edgecolor='white', bins=30, color='b', label='0')
    N1, bins1, patches1 = ax.hist(pred[np.where(labels == 1)], edgecolor='white', bins=30, color='r', label='1')
    
    #print(N0.sum())
    #print(N1.sum())
    #print('Bins0: ' + str(bins0))
    #print('Bins1: ' + str(bins1))
    
    plt.legend(loc='upper right')
    plt.ylabel('Occurrence')
    plt.xlabel('Predicted Probability')
    #plt.title('Histogram of Model Averaged Predicted Probabilities')
    
    # save figure
    path_figure = results_path + '/predictions_' + cohort + '.png'
    
    plt.savefig(path_figure)
    if show:
        plt.show()
    plt.close()
        
def compute_stats_and_plot_hist(scores, path, cohort, task, bootstrap, alpha=17):
    '''
    Output mean, median and confidence given the scores obtained via bootstrap resampling and plot histogram.
    Args:
        scores: 1-D array with scores
        path: path where to save stats and plot
        cohort: string, current cohort
        task: string, e.g. imaging
        bootstrap: int, number of bootstrap samples
        alpha: int, 100-alpha= confidence interval
    '''   
    # delete nans which may have occured due to unfortunate bootstrap sampling
    scores = np.delete(scores, np.where(np.isnan(scores)))
    
    # write results in txt
    with open(path + str(bootstrap) + '_stats_' + cohort, "a") as f:
        print('================================================================', file=f)
        print('50th percentile (median) = %.3f' % np.median(scores), file=f)
               
        # calculate e.g. 95% confidence intervals if alpha=5 (100 - alpha)
        # calculate lower percentile (e.g. 2.5)
        lower_p = alpha / 2.0
        # retrieve observation at lower percentile
        lower = max(0.0, np.percentile(scores, lower_p))
        print('%.1fth percentile = %.3f' % (lower_p, lower), file=f)
        # calculate upper percentile (e.g. 97.5)
        upper_p = (100 - alpha) + (alpha / 2.0)
        # retrieve observation at upper percentile
        upper = min(1.0, np.percentile(scores, upper_p))
        print('%.1fth percentile = %.3f' % (upper_p, upper), file=f)
        
        print('Mean: ' + str(np.mean(scores)), file=f)
        print('1.4*std: ' + str(1.4*np.std(scores)), file=f)
        print('2*std: ' + str(2*np.std(scores)), file=f)

    textstr = 'Median = ' + str(round(np.median(scores), 3)) + \
                '\n' + str(100-alpha) +'% HCI: ' + str(round(lower,3)) + ' - ' + str(round(upper,3))        
    plt.hist(scores, label=textstr)
    plt.title('Histogram of bootstrap resampling scores')
    plt.ylabel('Occurence')
    if task == 'classification':
        plt.xlabel('Model averaged AUC')
    if task == 'time2event':
        plt.xlabel('Model averaged HCI')  
        
    plt.legend(loc='upper right')
    
    plt.savefig(path + 'hist_' + cohort)
    #plt.show()
    plt.close()

def compute_pvalue_and_plot_km(times, labels, high_risk_patient_indices, low_risk_patient_indices, \
                               endpoint, path, cohort):
    
    # compute p-value from log-rank test to check whether there is a significant ground truth difference between the two groups
    results = logrank_test(times[high_risk_patient_indices], times[low_risk_patient_indices], \
                            labels[high_risk_patient_indices], labels[low_risk_patient_indices]) 

    ax = plt.subplot(111)
    
    kmf = KaplanMeierFitter()
    kmf.fit(times[high_risk_patient_indices]/365, event_observed=labels[high_risk_patient_indices], label="High Risk Group")
    kmf.plot(ax=ax, show_censors=True, ci_show=False)
    
    kmf.fit(times[low_risk_patient_indices]/365, event_observed=labels[low_risk_patient_indices], label="Low Risk Group")
    kmf.plot(ax=ax, show_censors=True, ci_show=False)
    ax.legend(loc='lower left', fontsize=12)
    if results.p_value < 1e-5:
        textstr = 'p-value < 1e-05'
    else:
        textstr = 'p-value = ' + str(round(results.p_value, 5))
    props = dict(boxstyle='round',facecolor='wheat', alpha=0.2)
    ax.text(-0.1, 0.28, textstr, fontsize=12, verticalalignment='top', bbox=props)
    ax.set_xlabel('Time (years)', fontsize=12)
    ax.set_yticks(np.arange(0, 1.01, 0.125))
    ax.set_ylim([0,1.01])
    if endpoint == 'OS':
        ax.set_ylabel('Survival probability', fontsize=12)
    if endpoint == 'DM':
        ax.set_ylabel('Metastasis-free probability', fontsize=12)
    if endpoint == 'LRF':
        ax.set_ylabel('Recurrence-free probability', fontsize=12)
    
    if cohort == 'maastro':
        ax.set_title('Kaplan Maier curves for MAASTRO patients')    
    if cohort == 'cro':
        ax.set_title('Kaplan Maier curves for CRO patients');
    if cohort == 'pmcc':
        ax.set_title('Kaplan Maier curves for PMH patients')         

    plt.savefig(path + 'km_curves_' + cohort)
    #plt.show()
    plt.close()  

def compute_pvalue_and_plot_km_paper(times, labels, high_risk_patient_indices, low_risk_patient_indices, \
                               endpoint, cohort, inputs, path):
    
    # compute p-value from log-rank test to check whether there is a significant ground truth difference between the two groups
    results = logrank_test(times[high_risk_patient_indices], times[low_risk_patient_indices], \
                            labels[high_risk_patient_indices], labels[low_risk_patient_indices]) 
    
    plt.rcParams.update({'font.size': 16})
    ax = plt.subplot(111)
    
    kmf = KaplanMeierFitter()
    kmf.fit(times[high_risk_patient_indices]/365, event_observed=labels[high_risk_patient_indices], label="(x) high risk group")
    kmf.plot(ax=ax, show_censors=True, ci_show=False, censor_styles={'marker': 'x'})
    
    kmf.fit(times[low_risk_patient_indices]/365, event_observed=labels[low_risk_patient_indices], label="(+) low risk group")
    kmf.plot(ax=ax, show_censors=True, ci_show=False, censor_styles={'marker': '+'})
    ax.legend(loc='center left')
    ax.get_legend().remove()
    if results.p_value < 1e-5:
        textstr = 'p-value < 1e-05'
    else:
        textstr = 'p-value = ' + str(round(results.p_value, 5))
        
    plt.text(0.23, 0.05, textstr, horizontalalignment='center', \
                 verticalalignment='center', transform=ax.transAxes)
    ax.set_xlabel('Time (y)', fontsize=16)
    ax.set_yticks(np.arange(0, 1.01, 0.125))
    ax.set_ylim([0,1.01])
       
    
    if inputs == 'imaging':
        if cohort == 'maastro':
            plt.text(0.78, 0.05, 'CNN - MAASTRO', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)    
       
        if cohort == 'cro':
            plt.text(0.78, 0.05, 'CNN - CRO', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)
        
        if cohort == 'pmcc':
            plt.text(0.78, 0.05, 'CNN - PMH', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)
    if inputs == 'clinical_variables':
        if cohort == 'maastro':
            plt.text(0.78, 0.05, 'ANN - MAASTRO', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)    
       
        if cohort == 'cro':
            plt.text(0.78, 0.05, 'ANN - CRO', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)
        
        if cohort == 'pmcc':
            plt.text(0.78, 0.05, 'ANN - PMH', horizontalalignment='center', \
                     verticalalignment='center', transform=ax.transAxes)

    plt.savefig(path + 'km_curves_' + cohort + '_' + inputs)
    #plt.show()
    plt.close() 
            