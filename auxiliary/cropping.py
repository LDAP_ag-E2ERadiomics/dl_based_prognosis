#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 17:17:49 2019

@author: Elia.Lombardo

Functions to perform central crops or to extract a box containg the GTV from the full CT slice using the masked CT slice. 
(adapted from https://codereview.stackexchange.com/questions/132914/crop-black-border-of-image-using-numpy)
"""

import numpy as np
import matplotlib.pyplot as plt

#%%



def crop_image_only_outside(ct, tol=0):
    """
    Function that return bouning box of ROI.
        ct: Input masked image. Can be either 2D or 3D 
        tol: Value above which bounding box will be defined
    """
    
    mask = ct>tol
    
    # 2D case (i.e. a slice)
    if ct.ndim==2:
        h,w = ct.shape
        mask_h,mask_w = mask.any(1),mask.any(0)
        
#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
    #    col_start,col_end = col_start-5,col_end+5 
    #    row_start,row_end = row_start-5,row_end+5
    
#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
            
        return ct[row_start:row_end, col_start:col_end]  
     
    # 3D case 
    if ct.ndim==3:
        d,h,w = ct.shape
        mask_d,mask_h,mask_w = mask.any((1,2)),mask.any((0,2)),mask.any((0,1)) # argument of any tells program along which dimension to look

#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))   
#        print('mask_d: ' + str(mask_d.argmax()))
#        print('d-mask_d[::-1]: ' + str(d-mask_d[::-1].argmax())) 
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
        depth_start,depth_end = mask_d.argmax(),d-mask_d[::-1].argmax()

#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
#        print('Cropped depth: ' + str(depth_end-depth_start))  
         
        return ct[depth_start:depth_end, row_start:row_end, col_start:col_end]
    
def crop_both_images_only_outside(ct, ct_full, tol=0):
    """
    Function that return bouning box of a masked CT and uses the same bounding box to crop also a full ct.
        ct: Input masked image. Can be either 2D or 3D 
        ct_full: Input full ct. Can be either 2D or 3D
        tol: Value above which bounding box will be defined
    """
    
    mask = ct>tol
    
    # 2D case (i.e. a slice)
    if ct.ndim==2:
        h,w = ct.shape
        mask_h,mask_w = mask.any(1),mask.any(0)
        
#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
    #    col_start,col_end = col_start-5,col_end+5 
    #    row_start,row_end = row_start-5,row_end+5
    
#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
            
        return ct[row_start:row_end, col_start:col_end], ct_full[row_start:row_end, col_start:col_end]  
     
    # 3D case 
    if ct.ndim==3:
        d,h,w = ct.shape
        mask_d,mask_h,mask_w = mask.any((1,2)),mask.any((0,2)),mask.any((0,1)) # argument of any tells program along which dimension to look

#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))   
#        print('mask_d: ' + str(mask_d.argmax()))
#        print('d-mask_d[::-1]: ' + str(d-mask_d[::-1].argmax())) 
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
        depth_start,depth_end = mask_d.argmax(),d-mask_d[::-1].argmax()

#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
#        print('Cropped depth: ' + str(depth_end-depth_start))  
         
        return ct[depth_start:depth_end, row_start:row_end, col_start:col_end], ct_full[depth_start:depth_end, row_start:row_end, col_start:col_end]


    
def crop_image_only_outside_bonds(ct, tol=0):
    """
    Function that returns the dimensions of bounding box of ROI.
        ct: Input masked image. Can be either 2D or 3D 
        tol: Value above which bounding box will be defined
    """
    
    mask = ct>tol
    
    # 2D case (i.e. a slice)
    if ct.ndim==2:
        h,w = ct.shape
        mask_h,mask_w = mask.any(1),mask.any(0)
        
#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
    #    col_start,col_end = col_start-5,col_end+5 
    #    row_start,row_end = row_start-5,row_end+5
    
#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
    
        cropped_width = col_end-col_start
        cropped_height = row_end-row_start
            
        return cropped_height, cropped_width  
     
    # 3D case 
    if ct.ndim==3:
        d,h,w = ct.shape
        mask_d,mask_h,mask_w = mask.any((1,2)),mask.any((0,2)),mask.any((0,1)) # argument of any tells program along which dimension to look

#        print('mask_w: ' + str(mask_w.argmax()))
#        print('w-mask_w[::-1]: ' + str(w-mask_w[::-1].argmax()))
#        print('mask_h: ' + str(mask_h.argmax()))
#        print('h-mask_h[::-1]: ' + str(h-mask_h[::-1].argmax()))   
#        print('mask_d: ' + str(mask_d.argmax()))
#        print('d-mask_d[::-1]: ' + str(d-mask_d[::-1].argmax())) 
        
        col_start,col_end = mask_w.argmax(),w-mask_w[::-1].argmax()
        row_start,row_end = mask_h.argmax(),h-mask_h[::-1].argmax()
        depth_start,depth_end = mask_d.argmax(),d-mask_d[::-1].argmax()

#        print('Cropped width: ' + str(col_end-col_start))
#        print('Cropped heigth: ' + str(row_end-row_start))
#        print('Cropped depth: ' + str(depth_end-depth_start))
        
        cropped_width = col_end-col_start
        cropped_height = row_end-row_start
        cropped_depth = depth_end-depth_start
         
        return cropped_depth, cropped_height, cropped_width

def central_crop(inputs, crop_size):
    "Crops volume to size crop_size around the center."
    
    shape = inputs.shape[2] # the width/height of the image
    
    # 2D case
    if inputs.ndim == 4:
        center = [shape//2, shape//2]
        
        # Calculate bounds
        target_width = int(crop_size/2.0)
        x_min = center[0] - target_width
        y_min = center[1] - target_width
        if shape % 2 == 1:
            target_width += 1
        x_max = center[0] + target_width
        y_max = center[1] + target_width
        
        # Return cropped volume
        return inputs[:,:,y_min:y_max,x_min:x_max]

        
    # 3D case   
    if inputs.ndim == 5:
        center = [shape//2, shape//2, shape//2]
        
        # Calculate bounds
        target_width = int(crop_size/2.0)
        x_min = center[0] - target_width
        y_min = center[1] - target_width
        z_min = center[2] - target_width
        if shape % 2 == 1:
            target_width += 1
        x_max = center[0] + target_width
        y_max = center[1] + target_width
        z_max = center[2] + target_width
        # Return cropped volume
        return inputs[:,:,z_min:z_max,y_min:y_max,x_min:x_max]
 
    
def rescale_ct_volume(inputs):
    min_bound = -1024.0
    max_bound = 400.0
    
    new_bound = (max_bound - min_bound) / 2.0
    shift     = new_bound - max_bound
    
    inputs = np.add(inputs, shift)
    inputs = np.divide(inputs, new_bound)
    return inputs


#%%
## 2D    
#cropped_test = crop_image_only_outside(ct[80, :, :], tol = -1)
#plt.imshow(cropped_test)
##3D
#cropped_test = crop_image_only_outside(ct, tol = -1)
#plt.imshow(cropped_test[20,:,10])
