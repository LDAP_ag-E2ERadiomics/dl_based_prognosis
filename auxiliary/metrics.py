#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 16:39:46 2020

@author: Elia.Lombardo

Custom metrics and callbacks with metrics
"""
from sklearn.metrics import roc_curve, auc, roc_auc_score
import tensorflow
import numpy as np
from warnings import warn
from lifelines.utils import concordance_index
import tensorflow.keras as keras 

# self written modules
import data_generators

def get_roc_metrics(y_true_arr, y_pred_arr, auc_only = False, remap_labels=False):
    
    # compute false positive rate (1 - specificity), true positive rate (sensitivity) 
    # and thresholds (sklearn just uses all possible thresholds from y_pred)
    falsepositiverate, truepositiverate, thresholds = roc_curve(y_true_arr, y_pred_arr, pos_label = 1)
    #print(thresholds)
    
    # compute the area under the receiver operating characteristic curve
    roc_auc = auc(falsepositiverate, truepositiverate)
    #print(roc_auc)
    
    if auc_only:
        return roc_auc
    else:
        return falsepositiverate, truepositiverate, thresholds, roc_auc 


def roc_auc_tf(y_true, y_pred):
    "Compute the area under the receiver curve and squash it into tensorflow."
    
    def try_roc_auc(y_true,y_pred):
        try:
            return roc_auc_score(y_true, y_pred)
        except ValueError:
            return 0
    return tensorflow.py_function(try_roc_auc, (y_true, y_pred), tensorflow.float32)


def hci(y_pred, breaks, time, event, yr):
    """Compute Harrell's concordance index metric.
    
    Parameters:
        y_pred: np.array with shape (nr_patients, nr_time_intervals) containing network predictions 
        breaks: np.array with shape (nr_time_intevals+1,) containg the time intervals
        time: np.array with shape (nr_patients,) containing times to event for different patients
        event: np.array with shape (nr_patients,) containing event labels (0 or 1) for different patients
        yr: time point (in years) at which to compute the concordance index
    """
    # for all patients compute the cumulative product of the conditional survival probabilities @ yr year 
    # -> one number per patient corresponding to survival probability (or no distant metastasis prob) at yr year
    some_yr_surv = np.cumprod(y_pred[:,0:np.where(breaks>yr*365)[0][0]], axis=1)[:,-1]
    
    # concordance_index compares survival probability ordering with survival time -> they should be in concordance (i.e. ideally CI=1)
    # ORP compares hazard and survival times so ideally CI=0
    return concordance_index(time, some_yr_surv, event)
    
class CheckpointAUC(keras.callbacks.Callback):
    """
    Class to compute Harrell's concordance index during training and save model according to it.
    Strongly inspired by Keras ModelCheckpoint Class.
    
    Parameters:
        train_data: training data with shape (b, c, d, h, w) for images
            or shape (nr_patients, nr_covariates) for clinical covariates
        train_events: np.array with shape (nr_patients,) containing event labels (0 or 1) for different patients.
        crop_size: has to be the same crop_size as in the main model (which is automatically stored in self.model).
        clinical_var: if True, clinical variables are combined with CNN
        train_clinical: training clinical covariates with shape (nr_patients, nr_covariates)
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        period: Interval (number of epochs) between checkpoints.
        inputs: either images ('imaging') o clincial variables ('clinical_variables')
    """
    
    def __init__(self, train_data, val_data, train_events, val_events,  \
                 filepath, inputs='imaging', crop_size=None, \
                 clinical_var=False, train_clinical=None, val_clinical=None, \
                 monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, \
                 mode='auto', period=1):
        
        self.train_data = train_data
        self.val_data = val_data
        self.train_events = train_events
        self.val_events = val_events
        self.inputs = inputs
        self.crop_size= crop_size        
        
        self.clinical_var = clinical_var
        if self.clinical_var:
            self.train_clinical = train_clinical
            self.val_clinical = val_clinical

        # set up generators for the prediction. batch_size=1 and no shuffle to use all the data
        self.dim = train_data.ndim -2
        #print('self.dim: ' + str(self.dim))
        self.channels = train_data.shape[1]        
        # data generators needed only for memory consuming cts
        if self.inputs == 'imaging' and self.clinical_var == False:
            self.train_generator = data_generators.ValDataGenerator((self.train_data, self.train_events), prediction=True, \
                                                                  batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
            self.val_generator = data_generators.ValDataGenerator((self.val_data, self.val_events), prediction=True, \
                                                                batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
        if self.inputs == 'imaging' and self.clinical_var == True:
            self.train_generator = data_generators.ValDataGenerator((self.train_data, self.train_events, self.train_clinical), prediction=True, \
                                                                  batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
            self.val_generator = data_generators.ValDataGenerator((self.val_data, self.val_events, self.val_clinical), prediction=True, \
                                                                batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
        super(CheckpointAUC, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.period = period
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            warn('ModelCheckpoint mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf        

    def on_epoch_end(self, epoch, logs={}):
        if self.inputs == 'imaging':
            y_train_predicted = np.asarray(self.model.predict(self.train_generator, \
                                                                        steps=self.train_generator.__len__(), \
                                                                        workers=2, use_multiprocessing=False))
                                                                        
            y_val_predicted = np.asarray(self.model.predict(self.val_generator, \
                                                                      steps=self.val_generator.__len__(), \
                                                                        workers=2, use_multiprocessing=False))
        if self.inputs == 'clinical_variables':
            y_train_predicted = np.asarray(self.model.predict(self.train_data, verbose=0))
                                                                        
            y_val_predicted = np.asarray(self.model.predict(self.val_data, verbose=0))
            
        # compute roc auc
        roc_auc_train = roc_auc_score(self.train_events, y_train_predicted)
        roc_auc_val = roc_auc_score(self.val_events, y_val_predicted)
        logs['auc'] = roc_auc_train
        logs['val_auc'] = roc_auc_val
        
        # save model parameters after given epoch
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            filepath = self.filepath.format(epoch=epoch + 1, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving model to %s'
                                  % (epoch + 1, self.monitor, self.best,
                                     current, filepath))
                        self.best = current
                        if self.save_weights_only:
                            self.model.save_weights(filepath, overwrite=True)
                        else:
                            self.model.save(filepath, overwrite=True)
                    else:
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s did not improve from %0.5f' %
                                  (epoch + 1, self.monitor, self.best))
            else:
                if self.verbose > 0:
                    print('\nEpoch %05d: saving model to %s' % (epoch + 1, filepath))
                if self.save_weights_only:
                    self.model.save_weights(filepath, overwrite=True)
                else:
                    self.model.save(filepath, overwrite=True)
                    
class CheckpointHCI(keras.callbacks.Callback):
    """
    Class to compute Harrell's concordance index during training and save model according to it.
    Strongly inspired by Keras ModelCheckpoint Class.
    
    Parameters:
        train_data: training data with shape (b, c, d, h, w) for images
            or shape (nr_patients, nr_covariates) for clinical covariates
        breaks: np.array with shape (nr_time_intevals+1,) containg the time intervals.
        train_times: np.array with shape (nr_patients,) containing times to event for different patients.
        train_events: np.array with shape (nr_patients,) containing event labels (0 or 1) for different patients.
        crop_size: has to be the same crop_size as in the main model (which is automatically stored in self.model).
        clinical_var: if True, clinical variables are combined with CNN
        train_clinical: training clinical covariates with shape (nr_patients, nr_covariates)
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        period: Interval (number of epochs) between checkpoints.
        inputs: either images ('imaging') o clincial variables ('clinical_variables')
    """
    
    def __init__(self, train_data, val_data, breaks, train_times, train_events, val_times, val_events,  \
                 filepath, inputs='imaging', crop_size=None, clinical_var=False, train_clinical=None, val_clinical=None, \
                 monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, \
                 mode='auto', period=1):
        
        self.train_data = train_data
        self.val_data = val_data
        self.breaks = breaks
        self.train_times = train_times
        self.train_events = train_events
        self.val_times = val_times
        self.val_events = val_events
        self.inputs = inputs
        self.crop_size= crop_size        
        
        self.clinical_var = clinical_var
        if self.clinical_var:
            self.train_clinical = train_clinical
            self.val_clinical = val_clinical
        #print('self.channels: ' + str(self.channels))
 
        # set up generators for the prediction. batch_size=1 and no shuffle to use all the data
        self.dim = train_data.ndim -2
        #print('self.dim: ' + str(self.dim))
        self.channels = train_data.shape[1]       
        # data generators needed only for memory consuming cts
        if self.inputs == 'imaging' and self.clinical_var == False:
            self.train_generator = data_generators.ValDataGenerator((self.train_data, self.train_events), prediction=True, \
                                                                  batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
            self.val_generator = data_generators.ValDataGenerator((self.val_data, self.val_events), prediction=True, \
                                                                batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
        if self.inputs == 'imaging' and self.clinical_var == True:
            self.train_generator = data_generators.ValDataGenerator((self.train_data, self.train_events, self.train_clinical), prediction=True, \
                                                                  batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
            self.val_generator = data_generators.ValDataGenerator((self.val_data, self.val_events, self.val_clinical), prediction=True, \
                                                                batch_size=1, crop_size=self.crop_size, clinical=self.clinical_var)
            
        super(CheckpointHCI, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.period = period
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            warn('ModelCheckpoint mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf        

    def on_epoch_end(self, epoch, logs={}):
        if self.inputs == 'imaging':
            y_train_predicted = np.asarray(self.model.predict(self.train_generator, \
                                                                        steps=self.train_generator.__len__(), \
                                                                        workers=2, use_multiprocessing=False))
                                                                        
            y_val_predicted = np.asarray(self.model.predict(self.val_generator, \
                                                                      steps=self.val_generator.__len__(), \
                                                                        workers=2, use_multiprocessing=False))
        if self.inputs == 'clinical_variables':
            y_train_predicted = np.asarray(self.model.predict(self.train_data, verbose=0))
                                                                        
            y_val_predicted = np.asarray(self.model.predict(self.val_data, verbose=0))
            
        # compute concordance index (e.g. after 1 year or after 3 years)
#        hci_1yr = HCI(y_train_predicted, self.breaks, self.train_times, self.train_events, 1)
#        val_hci_1yr = HCI(y_val_predicted, self.breaks, self.val_times, self.val_events, 1)
        hci_3yr = hci(y_train_predicted, self.breaks, self.train_times, self.train_events, 3)
        val_hci_3yr = hci(y_val_predicted, self.breaks, self.val_times, self.val_events, 3)
    
        #print("hci_1yr: {} - val_hci_1yr: {} - hci_3yr: {} - val_hci_3yr: {}".format(hci_1yr, val_hci_1yr, hci_3yr, val_hci_3yr))
#        logs['hci_1yr'] = hci_1yr
#        logs['val_hci_1yr'] = val_hci_1yr
        logs['hci_3yr'] = hci_3yr
        logs['val_hci_3yr'] = val_hci_3yr
        
        # save model parameters after given epoch
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            filepath = self.filepath.format(epoch=epoch + 1, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving model to %s'
                                  % (epoch + 1, self.monitor, self.best,
                                     current, filepath))
                        self.best = current
                        if self.save_weights_only:
                            self.model.save_weights(filepath, overwrite=True)
                        else:
                            self.model.save(filepath, overwrite=True)
                    else:
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s did not improve from %0.5f' %
                                  (epoch + 1, self.monitor, self.best))
            else:
                if self.verbose > 0:
                    print('\nEpoch %05d: saving model to %s' % (epoch + 1, filepath))
                if self.save_weights_only:
                    self.model.save_weights(filepath, overwrite=True)
                else:
                    self.model.save(filepath, overwrite=True)
