#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 15:13:48 2020

@author: Elia.Lombardo

Neural network architectures for different tasks and inputs
"""
from tensorflow.keras.layers import Activation, Conv2D, Conv3D, Dense, Dropout, Flatten, MaxPooling2D, MaxPooling3D, Input
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.initializers import Constant
from tensorflow.keras.layers import PReLU, concatenate
from tensorflow.keras import regularizers


def ANN_clinical(len_clinical, num_neurons_hl, l2, dropout, task, n_intervals=None):
    """
    Artificial neural netwrok to predict outcomes based on clincial variables.
    Args:
        len_clinical: number of clincial features, i.e. size of input layer
        num_neurons_hl: number of neurons in hidden layer
        l2: l2 regularization (weight decay) parameter
        dropout: dropout rate parameter
        task: either classification or time2event
        n_intervals: if task is time2event, this sets the number of output ime intervals
    """
    model = Sequential(name='ANN_clinical')
    
    model.add(Dense(num_neurons_hl, input_dim=len_clinical, bias_initializer='zeros', \
                    activation='relu', kernel_regularizer=regularizers.l2(l2))) 
    model.add(Dropout(rate=dropout))
    
    if task == 'classification':
        model.add(Dense(1))
    if task == 'time2event':
        model.add(Dense(n_intervals, input_dim=1, kernel_initializer='zeros', bias_initializer='zeros'))
        
    model.add(Activation('sigmoid'))
    
    return model

        
def CNN_2D(channels, crop_size, dropout_rate, task, n_intervals=None):
    " 2D CNN based on the paper by Diamant et al."
    model = Sequential(name="CNN_Diamant_2D")
    
    model.add(Conv2D(filters=32, kernel_size=5, input_shape=(crop_size, crop_size, channels)))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))
    
    model.add(Conv2D(filters=64, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))
    
    model.add(Conv2D(filters=128, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))
    
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(256))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(Dropout(rate=dropout_rate))

    if task == 'classification':
        model.add(Dense(1))
    if task == 'time2event':
        model.add(Dense(n_intervals, input_dim=1, kernel_initializer='zeros', bias_initializer='zeros'))

    model.add(Activation('sigmoid'))
    
    return model


def CNN_3D(channels, crop_size, dropout_rate, task, n_intervals=None):
    """
    3D extension to the Diamant et al. architecture.
    Args:
        crop_size: size to which the images are cropped, i.e. input size of the CNN
        dropout_rate: dropout rate parameter
        task: either classification or time2event
        n_intervals: if task is time2event, this sets the number of output ime intervals
    """
    model = Sequential(name="CNN_3D")
    
    model.add(Conv3D(filters=32, kernel_size=5, input_shape=(crop_size, crop_size, crop_size, channels)))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))
    
    model.add(Conv3D(filters=64, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))
    
    model.add(Conv3D(filters=128, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))
    
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(256))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(Dropout(rate=dropout_rate))
    
    if task == 'classification':
        model.add(Dense(1))
    if task == 'time2event':
        model.add(Dense(n_intervals, input_dim=1, kernel_initializer='zeros', bias_initializer='zeros'))

    model.add(Activation('sigmoid'))
    
    return model

def CNN_2D_no_top(channels, crop_size):
    " Convolutional layers for 2D CNN."
    model = Sequential(name="CNN_2D_no_top")
    
    model.add(Conv2D(filters=32, kernel_size=5, input_shape=(crop_size, crop_size, channels)))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))
    
    model.add(Conv2D(filters=64, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))
    
    model.add(Conv2D(filters=128, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling2D(pool_size=4, strides=4))   
    
    model.add(Flatten())

    return model

def CNN_3D_no_top(channels, crop_size):
    " Convolutional layers of CNN_3D. Later needed for CNN_plus_clinical_3D."
    model = Sequential(name="CNN_3D_no_top")
    
    model.add(Conv3D(filters=32, kernel_size=5, input_shape=(crop_size, crop_size, crop_size, channels)))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))
    
    model.add(Conv3D(filters=64, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))
    
    model.add(Conv3D(filters=128, kernel_size=3))
    model.add(PReLU(alpha_initializer=Constant(value=0.25)))
    model.add(MaxPooling3D(pool_size=4, strides=4))   
    
    model.add(Flatten())

    return model   
    
# Using Keras Functional API instead of Sequential Class
def CNN_plus_clinical_2D(channels, crop_size, len_clinical, dropout_rate, task, n_intervals=None):
    " 2D CNN combined with clinical covariates at FC layers."
    # clinical data input 
    clincial_data_input = Input(shape=(len_clinical,)) 
    # create CNN for cts
    cnn = CNN_2D_no_top(channels=channels, crop_size=crop_size)
    
    # create the input to our final set of layers as the *output* of the ct CNN and the clinical covariates
    combinedInput = concatenate([cnn.output, clincial_data_input])
    
    # our final FC layer head will have two dense layers, a dropout layer and a final sigmoid
    x = Dense(128)(combinedInput)
    x = Dense(256)(x)
    x = PReLU(alpha_initializer=Constant(value=0.25))(x)
    x = Dropout(rate=dropout_rate)(x)
    if task == 'classification':
        x = Dense(1)(x)
    if task == 'time2event':
        x = Dense(n_intervals, input_dim=1, kernel_initializer='zeros', bias_initializer='zeros')(x)
    y = Activation('sigmoid')(x)
    
    # the final model accepts treemaps and cts outputting a single value (prediction of some endpoint)
    model = Model(inputs=[cnn.input, clincial_data_input], outputs=y, name='2d_ct_plus_clinical_network')
    
    return model

def CNN_plus_clinical_3D(channels, crop_size, len_clinical, dropout_rate, task, n_intervals=None):
    " 3D CNN combined with clinical covariates at FC layers."
    # clinical data input 
    clincial_data_input = Input(shape=(len_clinical,)) 
    # create CNN for cts
    cnn = CNN_3D_no_top(channels=channels, crop_size=crop_size)
    
    # create the input to our final set of layers as the *output* of the ct CNN and the clinical covariates
    combinedInput = concatenate([cnn.output, clincial_data_input])
    
    # our final FC layer head will have two dense layers, a dropout layer and a final sigmoid
    x = Dense(128)(combinedInput)
    x = Dense(256)(x)
    x = PReLU(alpha_initializer=Constant(value=0.25))(x)
    x = Dropout(rate=dropout_rate)(x)
    if task == 'classification':
        x = Dense(1)(x)
    if task == 'time2event':
        x = Dense(n_intervals, input_dim=1, kernel_initializer='zeros', bias_initializer='zeros')(x)
    y = Activation('sigmoid')(x)
    
    # the final model accepts covariates and cts outputting a single value (prediction of certain endpoint)
    model = Model(inputs=[cnn.input, clincial_data_input], outputs=y, name='3d_ct_plus_clinical_network')
    
    return model