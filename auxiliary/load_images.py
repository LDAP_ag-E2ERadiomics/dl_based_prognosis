#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:32:39 2020

@author: Elia.Lombardo

Functions to load images and labels as input to CNNs
"""
import SimpleITK as sitk    # to work with .mha images
import numpy as np
from sklearn.model_selection import train_test_split
#%%

def load_mha(path_mha_file, window_min, window_max, output_min, output_max, binary, dim):
    "Helper function to load single .mha image, window and rescale it and in case make it binary or 2D/3D."
    #print(path_mha_file)
    itk = sitk.ReadImage(path_mha_file)

    windowing_filter=sitk.IntensityWindowingImageFilter()
    # window the images
    windowing_filter.SetWindowMinimum(window_min)
    windowing_filter.SetWindowMaximum(window_max)
    # rescale the images
    windowing_filter.SetOutputMinimum(output_min)
    windowing_filter.SetOutputMaximum(output_max)
    #print(windowing_filter.GetOutputMaximum())
    itk=windowing_filter.Execute(itk)  
    
    image = sitk.GetArrayFromImage(itk)
    
    # set everything but the smallest pixel value to the largest pixel value
    if binary:
        image[image > output_min] = output_max
        
    if dim == 3:    
        return image
    
    if dim == 2:
        # take only slice with highest number of GTV pixels (i.e. pixels with HU > - 1000)
        num_tumor_pixel = []
        slice_num = 0
        for axial_slice in range(0, image.shape[0]):
            num_tumor_pixel.append(np.sum(image[axial_slice,:,:] > output_min))
            
        num_tumor_pixel = np.array(num_tumor_pixel)  
        
        # find slice
        slice_num = np.argmax(num_tumor_pixel)
        #print(slice_num)    
        
        # keep only that slice
        image = image[slice_num,:,:]   
        #print('Shape: ' + str(image.shape)) # (height, width)
        
        return image      


def load_mhas(paths, window_min, window_max, output_min, output_max, binary=False, dim=3):
    "Load images for given paths and adjust dimensions to be network compatible."
    
    images = [load_mha(path, window_min, window_max, output_min, output_max, binary, dim) for path in paths]
    # convert from list to np.array
    images= np.array(images)
    #print('Shape of concatenated masked_cts (before adding the channel axis): ' + str(images.shape)) # (13,(256),256,256)
    
    # add newaxis for channels to obtain (batchsize, channels, (depth), height, width) shape
    images = images[:, None, ...]
    return images


def load_data(image_paths, labels, window_min=-500, window_max=500, \
              output_min=-1, output_max=1, binary=False, dim=3, channels=1):
    """
    Takes IDs for seg and ct and labels (and if wanted also clinical covariates themselves 
    and outputs the images and the labels as np.arrays.
    Args:
        image_paths: list with paths images, e.g. masked CTs
        labels: np.array with labels
        window_min: smallest windowing value
        window_max: largest windowing value
        output_min: snmallest rescaling value
        output_max: largest rescaling value
        binary: if True, all pixels other than the smallest value are set to the largest value
        dim: dimension of the images, either 2 or 3
    """

    print('Window min and max: %d/%d' % (window_min, window_max) )
    print('Output min and max: %d/%d' % (output_min, output_max) )
    print('First path of loaded data: ' + str(image_paths[0]))
    
    if channels != 1 :
        raise Exception ('Loading multichannel images could be implemented here.')
    
    else:
        images = load_mhas(image_paths, window_min, window_max, output_min, output_max, binary, dim)       
        print('Shape of cts: ' + str(images.shape))  # (full_b, c, (x), y, z)
      
        return images.astype(np.float32), labels.astype(np.float32)
        
            

def load_splitted_data(image_paths, labels, binary=False, dim=3, test_size=0.33):
    """
    Takes image paths and labels themselves and outputs train/test splitted images 
    and the labels as np.arrays and corresponding indices.
    """
    
    data, labels = load_data(image_paths, labels, binary, dim)
    
    indices = np.arange(len(labels))
    # Split the data set in a stratified and shuffled way (default, reproducible thanks to random_state=1)
    train_data, val_data, train_labels, val_labels, \
    train_idx, val_idx = train_test_split(data, \
                                          labels, \
                                          indices, \
                                          stratify=labels, \
                                          random_state=1, \
                                          test_size=test_size)
    
    return train_data, val_data, train_labels, val_labels, train_idx, val_idx

