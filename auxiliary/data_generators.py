#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 16:33:15 2020

@author: Elia.Lombardo

Module with training, validation and testing data loaders/generators
"""
import numpy as np
#import random
from tensorflow.keras.utils import Sequence

# self wirtten module
import cropping

# import MIC-DKFZ Dataloader
from batchgenerators.dataloading.data_loader import SlimDataLoaderBase

#%%
class TrainDataLoader(SlimDataLoaderBase):
    """
    Dataloader based on MIC_DKFZ SlimDataLoaderBase which supports multithreaded augmentations for train set.
    Batches are selected randomly.
    Args:
        data_set: tuple (of size 2) containing arrays of images and labels from which batches are generated. 
                  load_images.load_data function is used in main to generate this tuple.
                  if clinical = True, tuple (of size 3) also contains clinical covariates
        batch_size: integer, batch size
        clinical: bool, whether to include clinical covariates in the loader or not
        number_of_threads_in_multithreaded: needed to coordinate data loaders (?)
    """
    
    def __init__(self, data_set, batch_size, \
                 clinical=False, number_of_threads_in_multithreaded=None):
        self.data_set                           = data_set
        self.batch_size                         = batch_size
        self.clinical                           = clinical
        self.number_of_threads_in_multithreaded = number_of_threads_in_multithreaded
        self.n_samples                          = len(self.data_set[0])

      
        # Initialize super class with samples and labels stored in self._data
        super(TrainDataLoader, self).__init__(self.data_set, self.batch_size, self.number_of_threads_in_multithreaded)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.n_samples / self.batch_size))
    
    def generate_train_batch(self):
        "Generate the train batch using random instances of the full data (multithreading compatible)"

        # Select random instances of the data without replacement within an epoch
        if self.n_samples < self.batch_size:   
            raise Exception("\n Attention, the batch size is bigger than the total number of images. Please choose a batch size < " + str(self.n_samples) + ". \n")
            
        else:
            instances = np.random.choice(self.n_samples, self.batch_size, replace=False)
        
        #print('Len self._data: ' + str(len(self._data)))
        
        # Get data instances from super class. Both the images and the labels (and clinical data) are np.arrays        
        cts = self._data[0][instances]
        labels = self._data[1][instances] 
        if self.clinical:
            clinical = self._data[2][instances]
            return {'data':cts.astype(np.float32), 'label':labels.astype(np.float32), 'clinical':clinical.astype(np.float32)}
        # if no clinical variables are used just autput images and labels
        else:
            return {'data':cts.astype(np.float32), 'label':labels.astype(np.float32)}              
                


class TorchToKerasGenerator(Sequence):
    """
    Data generator to make MIC-DKFZ batch generator output work for Keras models.
    Args:
        batchgen: PyTorch data loader
        batches_per_epoch: number of batches per epoch, i.e. the output of batchgen.__len()
        clinical: if True, the data loader contains clinical covariatres additionally to images and labels
    """
    
    def __init__(self, batchgen, batches_per_epoch, clinical=False):
        self.batchgen = batchgen
        self.len = batches_per_epoch
        self.clinical = clinical  
        
    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.len

    def __getitem__(self, index):
        'Generate one batch of data'    
        batch = next(self.batchgen)
#        counter = 1
#        if counter == 1:
#            print('\n Shape of batch: ' + str(batch['data'].shape) + '\n')
#       counter += 1
        
        # get images
        X = batch['data']
        # move channel axis from 2nd to last to get the dimensions needed by Keras: (b, depth, height, width, c)
        X = np.moveaxis(X, 1, -1)         
                      
        # get labels
        y = batch['label']
        
        if self.clinical:
            # get covariates
            z = batch['clinical']
            return [X, z], y
        else:
            return X, y    



class ValDataGenerator(Sequence):
    """
    Data generator for Keras. Programmed explicitly for validation set or testing set --> no augmentations.
    Args:
        data_set: tuple with size 2 containing (data, labels). The shape of data = (b, x, y, z, c) 
        batch_size: batch size int
        clinical: if True, data_set tuple also contains clinical covariates, i.e. (data, labels, covariates)
        crop_size: size to which images are cropped in all dimensions
        shuffle: if True, indices for the batches are shuffled from one epoch to the other
        prediction: if True, the labels are not given as output --> testing
    """
    def __init__(self, data_set, batch_size, clinical=False, crop_size=256, \
                 shuffle=False, prediction=False):
        
        self.batch_size = batch_size
        self.crop_size = crop_size
        self.shuffle = shuffle
        self.prediction = prediction
        self.clinical = clinical
        self.data = data_set
        self.n_samples = len(self.data[0])
        self.on_epoch_end()
               
        
    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.n_samples / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        #print('Index: ' + str(index))
        
        if self.n_samples < self.batch_size:   
            raise Exception("\n Attention, the batch size is bigger than the total number of images. Please choose a batch size < " + str(self.n_samples) + ". \n")
        
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        #print('Indexes: ' + str(indexes))

        # get data 
        cts = self.data[0][indexes]
        labels = self.data[1][indexes] 
        if self.clinical:
            clinical = self.data[2][indexes]
            
        # crop the images as they won't go through the augmenter which performs the cropping for the  training samples
        cts = cropping.central_crop(cts, crop_size=self.crop_size)
        # move channel axis to get the dimensions needed by Keras: (b, x, y, z, c)
        cts = np.moveaxis(cts, 1, -1)
        
        if self.clinical:
            if self.prediction == False:
                return [cts.astype(np.float32), clinical.astype(np.float32)], labels.astype(np.float32)
            else:
                return [cts.astype(np.float32), clinical.astype(np.float32)]
        else:
            if self.prediction == False:
                return cts.astype(np.float32), labels.astype(np.float32)
            else:
                return cts.astype(np.float32)

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.data[0]))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
